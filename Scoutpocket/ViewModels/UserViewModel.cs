﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoutpocket.ViewModels
{
    public class UserViewModel
    {
        public int UserID { get; set; }

        public int ProfileID { get; set; }

        [Required(ErrorMessage = "{0} required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
         ErrorMessage = "Invalid email")]
        [Remote("CheckUserName", "User", ErrorMessage = "Already in use!")]
        [StringLength(100, ErrorMessage = "{0}: 100 is the limit")]
        public string UserMail { get; set; }

        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

        public int RoleID { get; set; }

        public virtual Profile profile { get; set; }

        [NotMapped]
        public HttpPostedFileBase MyFile { get; set; }
        [NotMapped]
        public string CroppedImagePath { get; set; }



        [DisplayName("Fornavn")]
        public string FirstName { get; set; }

  


        [DisplayName("Efternavn")]
        public string LastName { get; set; }

        [DisplayName("Telefon nummer")]
        public int Phone { get; set; }

        [DisplayName("")]
        public string Image { get; set; }


        //relations

        public virtual User user { get; set; }

        public UserViewModel()
        {
            this.user = user;
            this.profile = profile;
        }

        
        
        public virtual Player player { get; set; }

        public virtual Club club { get; set; }

        public virtual Subscriptions subscription { get; set; }





        //adding lists, so we can print properties, subProperties and ratings on the player profile thought the profile class.

        public List<Properties> properties { get; set; }

        public List<SubProperties> subProperties { get; set; }

        public List<Rating> rating { get; set; }

        public List<Profile> profiles { get; set; }





    }
}