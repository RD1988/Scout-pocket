﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Scoutpocket.ViewModels
{
    public class PropertiesViewModel
    {

    

        // gets a list of all the models needed on the page.

        public List<Properties> properties { get; set; }

        public List<SubProperties> subProperties { get; set; }

        public List<Rating> rating { get; set; }

        public List<Profile> profile { get; set; }



    }
}