﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoutpocket.ViewModels
{
    [MetadataType(typeof(ScoutMetaData))]
    public class ScoutProfileViewModel
    {


        //tuple viewmodel
        public Properties getProperties { get; set; }
        public SubProperties getSubProperties { get; set; }



        //Profile
        [Required(ErrorMessage = "Fornavn skal udfyldes.")]
        [DisplayName("Fornavn")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Efternavn skal udfyldes.")]
        [DisplayName("Efternavn")]
        public string LastName { get; set; }

        //User
        [Required(ErrorMessage = "E-mail adressen på kontaktpersonen skal udfyldes.")]
        [DisplayName("e-mail")]
        [Index("UserMailIndex", IsUnique = true)]
        public string UserMail { get; set; }

        [Required(ErrorMessage = "Adgangskoden skal udfyldes.")]
        [DataType(DataType.Password)]
        [DisplayName("adgangskode")]


        public string UserPassword { get; set; }
        public string UserPasswordRepeat { get; set; }
        public int RoleID { get; set; }

        public int Scout_ID { get; set; }

    }

    class ScoutMetaData
    {
        [Remote("IsUserExists", "Scout", ErrorMessage = "Denne e-mail adresse er allerede i brug")]
        public string UserMail { get; set; }
    }
}