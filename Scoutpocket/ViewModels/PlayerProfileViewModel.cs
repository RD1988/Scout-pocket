﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoutpocket.ViewModels
{
    [MetadataType(typeof(PlayerMetaData))]
    public class PlayerProfileViewModel
    {
        public HttpPostedFileBase MyFile { get; set; }

        public string CroppedImagePath { get; set; }

        public int PlayerID { get; set; }

        //Profile
        [Required(ErrorMessage = "Fornavn skal udfyldes.")]
        [DisplayName("Fornavn")]
        public string FirstName { get; set; } 

        [Required(ErrorMessage = "Efternavn skal udfyldes.")]
        [DisplayName("Efternavn")]
        public string LastName { get; set; }

        //club
        [Required(ErrorMessage = "Vælg venligst en klub")]

        //parse to clubID column in club.dbo
        public int ClubListID { get; set; }

        public string ClubName { get; set; }

        public int ID { get; set; }

        //the id from clublist
        public int Club_ID { get; set; }

        public int PositionID { get; set; }
        public int Position_ID { get; set; }

        public int SeriesID { get; set; }
        
        //public int Series_ID { get; set; }

        public int? SeriesID_Man { get; set; }

        public int? SeriesID_Women { get; set; }

        public int RegionID { get; set; }
        // Birth
        public DateTime BirthDate { get; set; }

        //User
        [Required(ErrorMessage = "E-mail adressen på kontaktpersonen skal udfyldes.")]
        [DisplayName("e-mail")]
        [Index("UserMailIndex", IsUnique = true)]
        public string UserMail { get; set; }

        [Required(ErrorMessage = "Adgangskoden skal udfyldes.")]
        [DataType(DataType.Password)]
        [DisplayName("adgangskode")]
        public string UserPassword { get; set; }




        [Required(ErrorMessage = "Adgangskoden skal gentages.")]
        [DataType(DataType.Password)]
        [DisplayName("Gentag adgangskode")]
        public string UserPasswordRepeat { get; set; }
        public int RoleID { get; set; }


        public virtual Player player { get; set; }
    }

    class PlayerMetaData
    {
        [Remote("IsUserExists", "Player", ErrorMessage = "Denne e-mail adresse er allerede i brug")]
        public string UserMail { get; set; }
    }
}