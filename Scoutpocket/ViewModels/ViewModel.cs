﻿using scoutpocket.Controllers;
using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.ViewModels
{
    public class GroupBy<K, T>
    {
        public K Key;
        public IEnumerable<T> Values;
    }

    public class ViewModel
    {



        public int ViewID { get; set; }
        public int PlayerID { get; set; }
        public int ClubID { get; set; }
        public DateTime Time { get; set; }
        public int ClubListID { get; set; }


        public string ClubName { get; set; }
        public string ClubImage { get; set; }
    }



}