﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.ViewModels
{
    public class SubPropertieViewModel
    {
        public int PositionID { get; set; }
        public int SubPropertiesID { get; set; }
        public string SubPropertiesName { get; set; }
        public string SubPropertiesDescription { get; set; }
        public int PropertiesID { get; set; }

        public Properties properties { get; set; }
        public Position position { get; set; }
    }
}