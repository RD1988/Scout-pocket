﻿using Scoutpocket.Models.Database;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Scoutpocket.ViewModels
{
    [MetadataType(typeof(ClubMetaData))]
    public class ClubProfileViewModel
    {
        //user
        [Required(ErrorMessage = "E-mail adressen på kontaktpersonen skal udfyldes.")]
        [DisplayName("e-mail")]
        [Index("UserMailIndex", IsUnique = true)]
        public string UserMail { get; set; }

        [Required(ErrorMessage = "Adgangskoden skal udfyldes.")]
        [DataType(DataType.Password)]
        [DisplayName("adgangskode")]


        public string UserPassword { get; set; }
        [DataType(DataType.Password)]
        public string UserPasswordRepeat { get; set; }
        public int RoleID { get; set; }

        //profile
        [Required(ErrorMessage = "Navnet på kontaktpersonen skal udfyldes.")]
        [DisplayName("navn")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Tlf. nr. på kontaktpersonen skal udfyldes.")]
        [DisplayName("telefonnummer")]
        public int Phone { get; set; }


        //club
        [Required(ErrorMessage = "Vælg venligst en klub")]

        //parse to clubID column in club.dbo
        public int ClubListID { get; set; }

        //the id from clublist
        [Key]
        public int Club_ID { get; set; }


    }

    class ClubMetaData
    {
        [Remote("IsUserExists", "Club", ErrorMessage = "Denne e-mail adresse er allerede i brug")]
        public string UserMail { get; set; }
    }
}