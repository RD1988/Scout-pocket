﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class ProfileViewModel
    {
        public int ProfileID { get; set; }

        public string FirstName { get; set; }

        public HttpPostedFileBase MyFile { get; set; }
        public string CroppedImagePath { get; set; }

        public bool isCurrentUser { get { return HttpContext.Current.User.Identity.Name == user.UserMail; } }
        public string LastName { get; set; }
        
        public int Phone { get; set; }

        public string Image { get; set; }
        public Profile CurrentUser { get; set; }
        //relations

        public virtual User user { get; set; }

        public virtual Player player { get; set; }

        public virtual Club club { get; set; }

        public virtual Subscriptions subscription { get; set; }

        public virtual List<Properties> properties { get; set; }
        public virtual List<SubProperties> subProperties { get; set; }
        public virtual List<Rating> ratings { get; set; }
        public virtual List<Profile> profiles { get; set; }




    }
}