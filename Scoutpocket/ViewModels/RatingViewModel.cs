﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Scoutpocket.ViewModels
{
    public class RatingViewModel
    {


        public int ProfileID { get; set; }

        [DisplayName("Fornavn")]
        public string FirstName { get; set; }


        [DisplayName("Efternavn")]
        public string LastName { get; set; }

        [DisplayName("Telefon nummer")]
        public int Phone { get; set; }

        public bool isCurrentUser { get
            {
                if(HttpContext.Current.User.Identity.Name == user.UserMail || player.ClubListID == CurrentUser.club.ClubListID)
                {
                    return true;
                }
                return false;
            }
        }

        //relations

        public virtual User user { get; set; }

        public virtual Player player { get; set; }

        public virtual Club club { get; set; }


        public string propertie { get; set; }



        public IEnumerable<Properties> properties { get; set; }

        public IEnumerable<SubProperties> subProperties { get; set; }

        public IEnumerable<Rating> rating { get; set; }

        public IQueryable<Profile> profile { get; set; }
        public List<PlayerPropertyScoutComment> Comments { get; internal set; }

        public List<List<string>> LineChartData
        {
            get
            {
                return GetLineChartData();

            }
            set
            {
            }
        }

        public Profile CurrentUser { get; internal set; }

        public List<List<string>> GetLineChartData()
        { 
            int getPlayer = (int)HttpContext.Current.Session["getProfileID"];
            int getPlayerPosition = (int)HttpContext.Current.Session["getPlayerPositionID"];

            List<string> listXDates = new List<string>();
            List<string> listYValues = new List<string>();
            List<string> listYAverage = new List<string>();

            foreach (var item in properties)
            {
                foreach (var item2 in item.SubProperties.Where(x => x.PositionID == Convert.ToInt32(getPlayerPosition)))
                {
                    var prop = rating.Where(x => x.SubPropertiesID == item2.SubPropertiesID && x.PlayerID == getPlayer).OrderBy(x => x.Date).ToList();

                    var playerId = Convert.ToInt32(getPlayer);
                    Player player = null;

                    try
                    {
                        player = profile.Where(p => p.player.PlayerID == playerId).First().player;
                    }
                    catch
                    {
                        //this might happen if the profile does not have a player
                        throw;
                    }

                    var seriesId = player?.SeriesID;
                    var position = Convert.ToInt32(getPlayerPosition);
                    var peopleInSameSeriesPosition = profile.Where(x => x.player.PositionID == position && x.player.SeriesID == seriesId).SelectMany(p => p.player.Ratings).ToList();
                    var ratingAverageForProp = peopleInSameSeriesPosition.Where(p => p.SubPropertiesID == item2.SubPropertiesID).Select(p =>
                    {
                        int value = 0;
                        int.TryParse(p.Value, out value);
                        return value;
                    });

                    double propAverage = 0.0;
                    if (ratingAverageForProp.Any())
                    {
                        propAverage = ratingAverageForProp.Average();
                        propAverage = Math.Round(propAverage, 1);
                    }
                    List<object[]> newProp = new List<object[]>();

                    //new way of making linechart (object x=all dates, object y=all values)
                    object[] xDates = new object[prop.Count];
                    object[] yValues = new object[prop.Count];
                    object[] yAverage = new object[prop.Count];
                    if (prop.Count > 0)
                    {

                        for (int i = 0; i < prop.Count; i++)
                        {
                            //x = ToShortDateString(), y = value
                            xDates[i] = prop[i].Date.Value.ToShortDateString();
                            yValues[i] = prop[i].Value;
                            yAverage[i] = propAverage;
                        }


                    }
                    var jsonXDates = Json.Encode(xDates);
                    var jsonYValues = Json.Encode(yValues);
                    var jsonAverage = Json.Encode(yAverage);

                    listXDates.Add(jsonXDates);
                    listYValues.Add(jsonYValues);
                    listYAverage.Add(jsonAverage);


                }
            }
            List<List<string>> lineChartData = new List<List<string>>();
            lineChartData.Add(listXDates);
            lineChartData.Add(listYValues);
            lineChartData.Add(listYAverage);

            return lineChartData;
            
        }
    }
}