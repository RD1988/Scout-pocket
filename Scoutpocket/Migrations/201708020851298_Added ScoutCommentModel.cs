namespace Scoutpocket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedScoutCommentModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlayerPropertyScoutComment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comment = c.String(),
                        PropertiesID = c.Int(nullable: false),
                        PlayerID = c.Int(nullable: false),
                        ScoutID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Player", t => t.PlayerID, cascadeDelete: false)
                .ForeignKey("dbo.Properties", t => t.PropertiesID, cascadeDelete: false)
                .ForeignKey("dbo.Scout", t => t.ScoutID, cascadeDelete: false)
                .Index(t => t.PropertiesID)
                .Index(t => t.PlayerID)
                .Index(t => t.ScoutID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlayerPropertyScoutComment", "ScoutID", "dbo.Scout");
            DropForeignKey("dbo.PlayerPropertyScoutComment", "PropertiesID", "dbo.Properties");
            DropForeignKey("dbo.PlayerPropertyScoutComment", "PlayerID", "dbo.Player");
            DropIndex("dbo.PlayerPropertyScoutComment", new[] { "ScoutID" });
            DropIndex("dbo.PlayerPropertyScoutComment", new[] { "PlayerID" });
            DropIndex("dbo.PlayerPropertyScoutComment", new[] { "PropertiesID" });
            DropTable("dbo.PlayerPropertyScoutComment");
        }
    }
}
