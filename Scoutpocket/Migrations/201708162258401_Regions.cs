namespace Scoutpocket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Regions : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Series", "SerieCategoryID", "dbo.SerieCategory");
            DropIndex("dbo.Series", new[] { "SerieCategoryID" });
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        RegionID = c.Int(nullable: false, identity: true),
                        RegionName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.RegionID);
            
            CreateTable(
                "dbo.SerieCategoryRegion",
                c => new
                    {
                        SerieCategory_SerieCategoryID = c.Int(nullable: false),
                        Region_RegionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SerieCategory_SerieCategoryID, t.Region_RegionID })
                .ForeignKey("dbo.SerieCategory", t => t.SerieCategory_SerieCategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Region", t => t.Region_RegionID, cascadeDelete: true)
                .Index(t => t.SerieCategory_SerieCategoryID)
                .Index(t => t.Region_RegionID);
            
            AddColumn("dbo.Series", "RegionID", c => c.Int(nullable: false));
            CreateIndex("dbo.Series", "RegionID");
            AddForeignKey("dbo.Series", "RegionID", "dbo.Region", "RegionID", cascadeDelete: true);
            DropColumn("dbo.Series", "SerieCategoryID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Series", "SerieCategoryID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Series", "RegionID", "dbo.Region");
            DropForeignKey("dbo.SerieCategoryRegion", "Region_RegionID", "dbo.Region");
            DropForeignKey("dbo.SerieCategoryRegion", "SerieCategory_SerieCategoryID", "dbo.SerieCategory");
            DropIndex("dbo.SerieCategoryRegion", new[] { "Region_RegionID" });
            DropIndex("dbo.SerieCategoryRegion", new[] { "SerieCategory_SerieCategoryID" });
            DropIndex("dbo.Series", new[] { "RegionID" });
            DropColumn("dbo.Series", "RegionID");
            DropTable("dbo.SerieCategoryRegion");
            DropTable("dbo.Region");
            CreateIndex("dbo.Series", "SerieCategoryID");
            AddForeignKey("dbo.Series", "SerieCategoryID", "dbo.SerieCategory", "SerieCategoryID", cascadeDelete: true);
        }
    }
}
