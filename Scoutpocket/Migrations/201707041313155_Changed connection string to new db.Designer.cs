// <auto-generated />
namespace Scoutpocket.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Changedconnectionstringtonewdb : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Changedconnectionstringtonewdb));
        
        string IMigrationMetadata.Id
        {
            get { return "201707041313155_Changed connection string to new db"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
