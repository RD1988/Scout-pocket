namespace Scoutpocket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class region : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SerieCategoryRegion", "SerieCategory_SerieCategoryID", "dbo.SerieCategory");
            DropForeignKey("dbo.SerieCategoryRegion", "Region_RegionID", "dbo.Region");
            DropForeignKey("dbo.Series", "RegionID", "dbo.Region");
            DropIndex("dbo.Series", new[] { "RegionID" });
            DropIndex("dbo.SerieCategoryRegion", new[] { "SerieCategory_SerieCategoryID" });
            DropIndex("dbo.SerieCategoryRegion", new[] { "Region_RegionID" });
            CreateTable(
                "dbo.SeriesRegion",
                c => new
                    {
                        Series_SeriesID = c.Int(nullable: false),
                        Region_RegionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Series_SeriesID, t.Region_RegionID })
                .ForeignKey("dbo.Series", t => t.Series_SeriesID, cascadeDelete: true)
                .ForeignKey("dbo.Region", t => t.Region_RegionID, cascadeDelete: true)
                .Index(t => t.Series_SeriesID)
                .Index(t => t.Region_RegionID);
            
            CreateTable(
                "dbo.SerieCategorySeries",
                c => new
                    {
                        SerieCategory_SerieCategoryID = c.Int(nullable: false),
                        Series_SeriesID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SerieCategory_SerieCategoryID, t.Series_SeriesID })
                .ForeignKey("dbo.SerieCategory", t => t.SerieCategory_SerieCategoryID, cascadeDelete: true)
                .ForeignKey("dbo.Series", t => t.Series_SeriesID, cascadeDelete: true)
                .Index(t => t.SerieCategory_SerieCategoryID)
                .Index(t => t.Series_SeriesID);
            
            AddColumn("dbo.Player", "Region_RegionID", c => c.Int());
            CreateIndex("dbo.Player", "Region_RegionID");
            AddForeignKey("dbo.Player", "Region_RegionID", "dbo.Region", "RegionID");
            DropColumn("dbo.Series", "RegionID");
            DropTable("dbo.SerieCategoryRegion");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SerieCategoryRegion",
                c => new
                    {
                        SerieCategory_SerieCategoryID = c.Int(nullable: false),
                        Region_RegionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SerieCategory_SerieCategoryID, t.Region_RegionID });
            
            AddColumn("dbo.Series", "RegionID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Player", "Region_RegionID", "dbo.Region");
            DropForeignKey("dbo.SerieCategorySeries", "Series_SeriesID", "dbo.Series");
            DropForeignKey("dbo.SerieCategorySeries", "SerieCategory_SerieCategoryID", "dbo.SerieCategory");
            DropForeignKey("dbo.SeriesRegion", "Region_RegionID", "dbo.Region");
            DropForeignKey("dbo.SeriesRegion", "Series_SeriesID", "dbo.Series");
            DropIndex("dbo.SerieCategorySeries", new[] { "Series_SeriesID" });
            DropIndex("dbo.SerieCategorySeries", new[] { "SerieCategory_SerieCategoryID" });
            DropIndex("dbo.SeriesRegion", new[] { "Region_RegionID" });
            DropIndex("dbo.SeriesRegion", new[] { "Series_SeriesID" });
            DropIndex("dbo.Player", new[] { "Region_RegionID" });
            DropColumn("dbo.Player", "Region_RegionID");
            DropTable("dbo.SerieCategorySeries");
            DropTable("dbo.SeriesRegion");
            CreateIndex("dbo.SerieCategoryRegion", "Region_RegionID");
            CreateIndex("dbo.SerieCategoryRegion", "SerieCategory_SerieCategoryID");
            CreateIndex("dbo.Series", "RegionID");
            AddForeignKey("dbo.Series", "RegionID", "dbo.Region", "RegionID", cascadeDelete: true);
            AddForeignKey("dbo.SerieCategoryRegion", "Region_RegionID", "dbo.Region", "RegionID", cascadeDelete: true);
            AddForeignKey("dbo.SerieCategoryRegion", "SerieCategory_SerieCategoryID", "dbo.SerieCategory", "SerieCategoryID", cascadeDelete: true);
        }
    }
}
