namespace Scoutpocket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcreatedtimestampforcomments : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlayerPropertyScoutComment", "Scout_UserID", "dbo.User");
            DropIndex("dbo.PlayerPropertyScoutComment", new[] { "Scout_UserID" });
            AddColumn("dbo.PlayerPropertyScoutComment", "Created", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PlayerPropertyScoutComment", "Comment", c => c.String(nullable: false));
            AlterColumn("dbo.PlayerPropertyScoutComment", "Scout_UserID", c => c.Int(nullable: false));
            CreateIndex("dbo.PlayerPropertyScoutComment", "Scout_UserID");
            AddForeignKey("dbo.PlayerPropertyScoutComment", "Scout_UserID", "dbo.User", "UserID", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlayerPropertyScoutComment", "Scout_UserID", "dbo.User");
            DropIndex("dbo.PlayerPropertyScoutComment", new[] { "Scout_UserID" });
            AlterColumn("dbo.PlayerPropertyScoutComment", "Scout_UserID", c => c.Int());
            AlterColumn("dbo.PlayerPropertyScoutComment", "Comment", c => c.String());
            DropColumn("dbo.PlayerPropertyScoutComment", "Created");
            CreateIndex("dbo.PlayerPropertyScoutComment", "Scout_UserID");
            AddForeignKey("dbo.PlayerPropertyScoutComment", "Scout_UserID", "dbo.User", "UserID");
        }
    }
}
