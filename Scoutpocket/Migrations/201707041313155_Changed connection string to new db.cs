namespace Scoutpocket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changedconnectionstringtonewdb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClubList",
                c => new
                    {
                        ClubListID = c.Int(nullable: false, identity: true),
                        ClubName = c.String(),
                        ClubImage = c.String(),
                    })
                .PrimaryKey(t => t.ClubListID);
            
            CreateTable(
                "dbo.Player",
                c => new
                    {
                        PlayerID = c.Int(nullable: false),
                        ClubListID = c.Int(nullable: false),
                        SeriesID = c.Int(),
                        PositionID = c.Int(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PlayerID)
                .ForeignKey("dbo.ClubList", t => t.ClubListID, cascadeDelete: true)
                .ForeignKey("dbo.Profile", t => t.PlayerID)
                .ForeignKey("dbo.Series", t => t.SeriesID)
                .Index(t => t.PlayerID)
                .Index(t => t.ClubListID)
                .Index(t => t.SeriesID);
            
            CreateTable(
                "dbo.Profile",
                c => new
                    {
                        ProfileID = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Phone = c.Int(nullable: false),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.ProfileID)
                .ForeignKey("dbo.User", t => t.ProfileID)
                .Index(t => t.ProfileID);
            
            CreateTable(
                "dbo.Club",
                c => new
                    {
                        ClubID = c.Int(nullable: false),
                        ClubListID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClubID)
                .ForeignKey("dbo.ClubList", t => t.ClubListID, cascadeDelete: true)
                .ForeignKey("dbo.Profile", t => t.ClubID)
                .Index(t => t.ClubID)
                .Index(t => t.ClubListID, unique: true, name: "TitleIndex");
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        SubscriptionID = c.Int(nullable: false),
                        SubscriptionIDEpay = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        TransactionID = c.Int(nullable: false),
                        OrderID = c.String(),
                        Active = c.Boolean(nullable: false),
                        Category = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SubscriptionID)
                .ForeignKey("dbo.Profile", t => t.SubscriptionID)
                .Index(t => t.SubscriptionID);
            
            CreateTable(
                "dbo.PreparedSubscriptions",
                c => new
                    {
                        PrepId = c.Int(nullable: false),
                        SubscriptionIDEpay = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        TransactionID = c.Int(nullable: false),
                        OrderID = c.String(),
                        Active = c.Boolean(nullable: false),
                        Category = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PrepId)
                .ForeignKey("dbo.Subscriptions", t => t.PrepId)
                .Index(t => t.PrepId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserMail = c.String(nullable: false, maxLength: 100),
                        UserPassword = c.String(),
                        RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.Rating",
                c => new
                    {
                        RatingID = c.Int(nullable: false, identity: true),
                        PlayerID = c.Int(nullable: false),
                        SubPropertiesID = c.Int(nullable: false),
                        Value = c.String(),
                        ScoutID = c.Int(nullable: false),
                        Birth = c.DateTime(),
                        Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.RatingID)
                .ForeignKey("dbo.Player", t => t.PlayerID, cascadeDelete: true)
                .ForeignKey("dbo.SubProperties", t => t.SubPropertiesID, cascadeDelete: true)
                .Index(t => t.PlayerID)
                .Index(t => t.SubPropertiesID);
            
            CreateTable(
                "dbo.Series",
                c => new
                    {
                        SeriesID = c.Int(nullable: false, identity: true),
                        SeriesName = c.String(),
                        SerieCategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SeriesID)
                .ForeignKey("dbo.SerieCategory", t => t.SerieCategoryID, cascadeDelete: true)
                .Index(t => t.SerieCategoryID);
            
            CreateTable(
                "dbo.View",
                c => new
                    {
                        ViewID = c.Int(nullable: false, identity: true),
                        PlayerID = c.Int(nullable: false),
                        ClubID = c.Int(nullable: false),
                        Time = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ViewID)
                .ForeignKey("dbo.Player", t => t.PlayerID, cascadeDelete: true)
                .Index(t => t.PlayerID);
            
            CreateTable(
                "dbo.Gender",
                c => new
                    {
                        GenderID = c.Int(nullable: false, identity: true),
                        GenderType = c.String(),
                    })
                .PrimaryKey(t => t.GenderID);
            
            CreateTable(
                "dbo.SerieCategory",
                c => new
                    {
                        SerieCategoryID = c.Int(nullable: false, identity: true),
                        SerieCategoryName = c.String(),
                        GenderID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SerieCategoryID)
                .ForeignKey("dbo.Gender", t => t.GenderID, cascadeDelete: true)
                .Index(t => t.GenderID);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        Price = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        OrderIDEpay = c.String(),
                        Brought = c.DateTime(nullable: false),
                        Product = c.String(),
                    })
                .PrimaryKey(t => t.OrderID)
                .ForeignKey("dbo.User", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Position",
                c => new
                    {
                        PositionID = c.Int(nullable: false, identity: true),
                        PositionName = c.String(),
                        PositionCategory = c.String(),
                    })
                .PrimaryKey(t => t.PositionID);
            
            CreateTable(
                "dbo.SubProperties",
                c => new
                    {
                        SubPropertiesID = c.Int(nullable: false, identity: true),
                        SubPropertiesName = c.String(),
                        SubPropertiesDescription = c.String(),
                        PropertiesID = c.Int(nullable: false),
                        PositionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SubPropertiesID)
                .ForeignKey("dbo.Position", t => t.PositionID, cascadeDelete: true)
                .ForeignKey("dbo.Properties", t => t.PropertiesID, cascadeDelete: true)
                .Index(t => t.PropertiesID)
                .Index(t => t.PositionID);
            
            CreateTable(
                "dbo.Properties",
                c => new
                    {
                        PropertiesID = c.Int(nullable: false, identity: true),
                        PropertiesName = c.String(),
                    })
                .PrimaryKey(t => t.PropertiesID);
            
            CreateTable(
                "dbo.Scout",
                c => new
                    {
                        ScoutID = c.Int(nullable: false, identity: true),
                        profile_ProfileID = c.Int(),
                    })
                .PrimaryKey(t => t.ScoutID)
                .ForeignKey("dbo.Profile", t => t.profile_ProfileID)
                .Index(t => t.profile_ProfileID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Scout", "profile_ProfileID", "dbo.Profile");
            DropForeignKey("dbo.Rating", "SubPropertiesID", "dbo.SubProperties");
            DropForeignKey("dbo.SubProperties", "PropertiesID", "dbo.Properties");
            DropForeignKey("dbo.SubProperties", "PositionID", "dbo.Position");
            DropForeignKey("dbo.Order", "UserID", "dbo.User");
            DropForeignKey("dbo.SerieCategory", "GenderID", "dbo.Gender");
            DropForeignKey("dbo.Series", "SerieCategoryID", "dbo.SerieCategory");
            DropForeignKey("dbo.View", "PlayerID", "dbo.Player");
            DropForeignKey("dbo.Player", "SeriesID", "dbo.Series");
            DropForeignKey("dbo.Rating", "PlayerID", "dbo.Player");
            DropForeignKey("dbo.Profile", "ProfileID", "dbo.User");
            DropForeignKey("dbo.Subscriptions", "SubscriptionID", "dbo.Profile");
            DropForeignKey("dbo.PreparedSubscriptions", "PrepId", "dbo.Subscriptions");
            DropForeignKey("dbo.Player", "PlayerID", "dbo.Profile");
            DropForeignKey("dbo.Club", "ClubID", "dbo.Profile");
            DropForeignKey("dbo.Club", "ClubListID", "dbo.ClubList");
            DropForeignKey("dbo.Player", "ClubListID", "dbo.ClubList");
            DropIndex("dbo.Scout", new[] { "profile_ProfileID" });
            DropIndex("dbo.SubProperties", new[] { "PositionID" });
            DropIndex("dbo.SubProperties", new[] { "PropertiesID" });
            DropIndex("dbo.Order", new[] { "UserID" });
            DropIndex("dbo.SerieCategory", new[] { "GenderID" });
            DropIndex("dbo.View", new[] { "PlayerID" });
            DropIndex("dbo.Series", new[] { "SerieCategoryID" });
            DropIndex("dbo.Rating", new[] { "SubPropertiesID" });
            DropIndex("dbo.Rating", new[] { "PlayerID" });
            DropIndex("dbo.PreparedSubscriptions", new[] { "PrepId" });
            DropIndex("dbo.Subscriptions", new[] { "SubscriptionID" });
            DropIndex("dbo.Club", "TitleIndex");
            DropIndex("dbo.Club", new[] { "ClubID" });
            DropIndex("dbo.Profile", new[] { "ProfileID" });
            DropIndex("dbo.Player", new[] { "SeriesID" });
            DropIndex("dbo.Player", new[] { "ClubListID" });
            DropIndex("dbo.Player", new[] { "PlayerID" });
            DropTable("dbo.Scout");
            DropTable("dbo.Properties");
            DropTable("dbo.SubProperties");
            DropTable("dbo.Position");
            DropTable("dbo.Order");
            DropTable("dbo.SerieCategory");
            DropTable("dbo.Gender");
            DropTable("dbo.View");
            DropTable("dbo.Series");
            DropTable("dbo.Rating");
            DropTable("dbo.User");
            DropTable("dbo.PreparedSubscriptions");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.Club");
            DropTable("dbo.Profile");
            DropTable("dbo.Player");
            DropTable("dbo.ClubList");
        }
    }
}
