namespace Scoutpocket.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changedscouttouser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlayerPropertyScoutComment", "ScoutID", "dbo.Scout");
            DropIndex("dbo.PlayerPropertyScoutComment", new[] { "ScoutID" });
            RenameColumn(table: "dbo.PlayerPropertyScoutComment", name: "ScoutID", newName: "Scout_UserID");
            AlterColumn("dbo.PlayerPropertyScoutComment", "Scout_UserID", c => c.Int());
            CreateIndex("dbo.PlayerPropertyScoutComment", "Scout_UserID");
            AddForeignKey("dbo.PlayerPropertyScoutComment", "Scout_UserID", "dbo.User", "UserID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlayerPropertyScoutComment", "Scout_UserID", "dbo.User");
            DropIndex("dbo.PlayerPropertyScoutComment", new[] { "Scout_UserID" });
            AlterColumn("dbo.PlayerPropertyScoutComment", "Scout_UserID", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.PlayerPropertyScoutComment", name: "Scout_UserID", newName: "ScoutID");
            CreateIndex("dbo.PlayerPropertyScoutComment", "ScoutID");
            AddForeignKey("dbo.PlayerPropertyScoutComment", "ScoutID", "dbo.Scout", "ScoutID", cascadeDelete: false);
        }
    }
}
