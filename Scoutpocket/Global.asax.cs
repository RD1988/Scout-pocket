﻿using Hangfire;
using Scoutpocket.Models.Database;
using Scoutpocket.RecurringJobs;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Scoutpocket
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private const String ReturnUrlRegexPattern = @"\?ReturnUrl=.*$";

        public MvcApplication()
        {
            PreSendRequestHeaders += MvcApplicationOnPreSendRequestHeaders;
        }

        private void MvcApplicationOnPreSendRequestHeaders(object sender, EventArgs e)
        {
            String redirectUrl = Response.RedirectLocation;

            if (String.IsNullOrEmpty(redirectUrl) || !Regex.IsMatch(redirectUrl, ReturnUrlRegexPattern))
            {

                return;

            }

            Response.RedirectLocation = Regex.Replace(redirectUrl, ReturnUrlRegexPattern, String.Empty);


        }

        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Hangfire initialize
            // GlobalConfiguration.Configuration.UseSqlServerStorage(@"Data Source=mssql7.unoeuro.com;Initial Catalog=scoutpocket_dk_db;Persist Security Info=True;User ID=scoutpocket_dk;Password=42607056;MultipleActiveResultSets=True;Application Name=EntityFramework");

            //GlobalConfiguration.Configuration.UseSqlServerStorage(@"Data Source=X220-PC\SQLEXPRESS;Initial Catalog=scoutpocket_10_09;Integrated Security=True");

            GlobalConfiguration.Configuration.UseSqlServerStorage(@"Server=tcp:scoutuno.database.windows.net,1433;Initial Catalog=Scoutpocket;Persist Security Info=False;User ID=hamun;Password=S-KodenEr123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

            //Configure logging - currently set to output to visual studio debug cmd line
            //LogProvider.SetCurrentLogProvider(new CustomHangfireLogProvider());

            //Start hangfire
            var server = new BackgroundJobServer();

            //Add jobs

            // Cron = monthly at 00:00 first day of month
            RecurringJob.AddOrUpdate(() => HangfireRoutines.AuthorizeAllSubscriptions(), "0 0 1 * *");

            // Cron = at "season change" 01:00 first day of month, Jan and July
            RecurringJob.AddOrUpdate(() => HangfireRoutines.UpdateSubscriptions(), "0 1 1 1,7 *");
            
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.Url.Host.StartsWith("www") && !Request.Url.IsLoopback)
            {
                UriBuilder builder = new UriBuilder(Request.Url);
                builder.Host = "www." + Request.Url.Host;
                Response.Redirect(builder.ToString(), true);
            }
        }

        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {


            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        string userMail = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;

                        using (Connection db = new Connection())
                        {
                            User user = db.Users.SingleOrDefault(a => a.UserMail == userMail);
                            roles = user.RoleID.ToString();
                        }

                        e.User = new System.Security.Principal.GenericPrincipal(
                        new System.Security.Principal.GenericIdentity(userMail, "Forms"), roles.Split(';'));


                    }
                    catch (Exception)
                    {

                        // throw;
                    }
                }
            }

        }



        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    Exception exception = Server.GetLastError();
        //    Response.Clear();

        //    var httpException = exception as HttpException;

        //    if (httpException != null)
        //    {
        //        string action;

        //        switch (httpException.GetHttpCode())
        //        {
        //            case 404:
        //                // page not found
        //                action = "NotFound";
        //                break;
        //            case 403:
        //                // forbidden
        //                action = "Forbidden";
        //                break;
        //            case 500:
        //                // server error
        //                action = "HttpError500";
        //                break;
        //            default:
        //                action = "Unknown";
        //                break;
        //        }

        //        // clear error on server
        //        Server.ClearError();

        //        Response.Redirect(String.Format("~/Errors/{0}", action));
        //    }
        //    else
        //    {
        //        // this is my modification, which handles any type of an exception.
        //        Response.Redirect(String.Format("~/Errors/Unknown"));
        //    }
        //}




    }
}
