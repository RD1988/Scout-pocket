﻿using Scoutpocket.Models.Database;
using System.Net.Mail;
using System.Web.Mvc;

namespace Scoutpocket.Controllers
{
    public class HomeController : Controller
    {
        Connection db = new Connection();

        public ActionResult Index()
        {
            return View();
        }

        //[Route("logind")]
        //public ActionResult Logind()
        //{
        //    return View();
        //}

        public ActionResult DemoVideo()
        {
            return View();
        }


        public ActionResult DemoTest()
        {
            return View();
        }

        public ActionResult CoverImage()
        {
            return View();
        }







        // GET: Player
        [Route("spiller")]
        public ActionResult Player()
        {
            return View();
        }

        [Route("klub")]
        public ActionResult Club()
        {
            return View();
        }

        [Route("handelsbetingelser")]
         public ActionResult Conditions()
        {


            return View();
        }

      
        [Route("kontakt")]
        public ActionResult Contact()
        {
           
            return View();
        }


  
        public ActionResult SendMail()
        {
            return View();
        }



    
        [HttpPost]
        public ActionResult SendMail(FormCollection formContact)
        {
            string firstname = formContact.Get("firstName");
            string email = formContact.Get("email");
            string body = formContact.Get("body");
            string message = formContact.Get("message");


            MailMessage mailMessage = new MailMessage();

            MailAddress fromAddress = new MailAddress(email, firstname);

            mailMessage.From = fromAddress;

            mailMessage.To.Add("scoutpocket@outlook.dk");

            //scoutpocket @outlook.dk
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = body;
          
            

            mailMessage.Body = message;
            

            SmtpClient smtpClient = new SmtpClient();
            
            
            smtpClient.Send(mailMessage);

            //return View("");

            return RedirectToAction("Contact", "Home");
        }





        [Route("samarbejdspartnere")]
        public ActionResult Collaborators()
        {
            return View();
        }

    }
}