﻿using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.Models.Repository;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoutpocket.Controllers
{
    public class SubPropertiesController : Controller
    {
        private Connection db = new Connection();

        ISubPropertieRepository _repository;

        public SubPropertiesController() : this(new SubPropertieRepository())
        {

        }

        public SubPropertiesController(ISubPropertieRepository repository)
        {
            _repository = repository;
        }

        [Authorize(Roles = "1")]
        public ActionResult AddSubProperties()
        {
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "PositionName", "PositionCategory", 0);
            ViewBag.PropertiesID = new SelectList(db.Propertiess, "PropertiesID", "PropertiesName");

            return View();
        }

        [Authorize(Roles = "1")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddSubProperties(SubPropertieViewModel viewModel, Position position, Properties properties)
        {
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "PositionName", "PositionCategory", position.PositionID);
            ViewBag.PropertiesID = new SelectList(db.Propertiess, "PropertiesID", "PropertiesName", properties.PropertiesID);

            try
            {
                if (ModelState.IsValid)
                {
                    _repository.CreateSubProperties(viewModel, position, properties);
                    ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "PositionName", "PositionCategory", position.PositionID);
                    ViewBag.PropertiesID = new SelectList(db.Propertiess, "PropertiesID", "PropertiesName", properties.PropertiesID);

                    ViewBag.Succes = "Egenskab oprettet";
                }

            }

            catch (Exception ex)
            {

                ModelState.AddModelError("", ex);
                ViewData["CreateError"] = "Unable to create; view innerexception";
            }

            return View();
        }
    }
}