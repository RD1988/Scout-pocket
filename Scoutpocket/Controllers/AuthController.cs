﻿using Scoutpocket.Models.Database;
using System;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace Scoutpocket.Controllers
{
    public class AuthController : Controller
    {
        private Connection db = new Connection();
        [Route("logind")]
        // GET: Auth login
        public ActionResult Logind()
        {
            return View();
        }


        public string encryption(String password)
        {

            if (!string.IsNullOrEmpty(password))
            {
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] encrypt;
                UTF8Encoding encode = new UTF8Encoding();
                //encrypt the given password string into Encrypted data  

                encrypt = md5.ComputeHash(encode.GetBytes(password));

                StringBuilder encryptdata = new StringBuilder();
                //Create a new string by using the encrypted data  
                for (int i = 0; i < encrypt.Length; i++)
                {
                    encryptdata.Append(encrypt[i].ToString());
                }

                return encryptdata.ToString();

            }

            else
            {

                ViewBag.Error = "Du mangler at indtaske en adgangskode";
            }



            return null;




        }

        [HttpPost]
        [Route("logind")]
        public ActionResult Logind(User model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {
                using (Connection db = new Connection())
                {
                    string userMail = model.UserMail;
                    string userPasseword = null;


                    //check if the code is with md5 encryption?
                    //this should be deleted

                    userPasseword = encryption(model.UserPassword);


                    //checks if the user is valid equals mail to the password.

                    //first with the encryption
                    bool isUserValid = db.Users.Any(user => user.UserMail == userMail && user.UserPassword == userPasseword);

                    if (!isUserValid)
                    {
                        //if it does not work then it might be a user without the encryption.
                        isUserValid = db.Users.Any(user => user.UserMail == userMail && user.UserPassword == model.UserPassword);
                    }

                    User u = db.Users.FirstOrDefault(user => user.UserMail == userMail && user.UserPassword == userPasseword);

                    if (u != null && u.RoleID != 1)
                    {
                        Session["getPlayerID"] = u.profile.ProfileID;
                    }


                    if (isUserValid)
                    {
                        FormsAuthentication.SetAuthCookie(userMail, false);


                        if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                           && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Role", "Auth");
                        }

                    }
                    else 
                    {
                        ViewBag.Error = "Ugyldig e-mail eller adgangskode";
                    }

                }
            }

            return View(model);
        }

        public ActionResult Role()
        {
            string userMail = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            string roles = string.Empty;

            using (Connection db = new Connection())
            {
                //An error will occor here if you have the same user multiple times.
                //Just use the first one instead
                User user = db.Users.FirstOrDefault(a => a.UserMail == userMail);
                roles = user.RoleID.ToString();

                if (roles == "1")
                {
                    return RedirectToAction("Dashboard", "Administration");
                }

                else if (roles == "2")
                {

                    return RedirectToAction("ClubProfile", "Club");
                }

                else if (roles == "3")
                {

                    return RedirectToAction("playerProfile", "Player");
                }

                else if (roles == "4")
                {

                    return RedirectToAction("scoutProfile", "Scout");
                }


            }


            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        [Route("glemt-adgangskode")]
        public ActionResult ForgotPassword()
        {
            return View();
        }



        [HttpPost]
        [Route("glemt-adgangskode")]
        public ActionResult ForgotPassword(User model)
        {


            int lengthOfPassword = 8;
            string guid = Guid.NewGuid().ToString().Replace("-", "");

            string newPass = guid.Substring(0, lengthOfPassword);



            if (ModelState.IsValid)
            {
                using (Connection db = new Connection())
                {
                    string userMail = model.UserMail;

                    bool userValid = db.Users.Any(u => u.UserMail == userMail);

                    if (userValid)
                    {
                        User ur = db.Users.FirstOrDefault(x => x.UserMail == userMail);

                        if (ur != null)
                        {
                            ur.UserPassword = encryption(newPass);
                            db.SaveChanges();

                        }

                        //string lostPassword;

                        //newPass = ur.UserPassword;




                        MailMessage mailMessage = new MailMessage();

                        MailAddress fromAddress = new MailAddress("scoutpocket@outlook.dk");

                        mailMessage.From = fromAddress;

                        mailMessage.To.Add(userMail);

                        mailMessage.Body = "Hej<br/><br/> Din nye adgangskode er følgende: " + newPass + " <br/><br/> Hvis du stadig oplever problemer med dit login kan du kontakte os på denne mail.<br/><br/> Med venlig hilsen<br/> Scout Pocket";

                        mailMessage.IsBodyHtml = true;
                        mailMessage.Subject = "Glemt adgangskode";

                        SmtpClient smtpClient = new SmtpClient();

                        smtpClient.Host = "smtp.unoeuro.com";

                        smtpClient.Port = 25;

                        smtpClient.Send(mailMessage);

                        ViewBag.Succes = "Vi har sendt dig en e-mail med din nye adgangskode";
                    }

                    else
                    {

                        ViewBag.Error = "Brugeren findes ikke.";
                    }

                }

            }

            return View();
        }




    }
}