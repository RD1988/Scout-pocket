﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scoutpocket.Controllers
{
    public class DemoController : Controller
    {


        public void StateDropDownList()
        {
            List<SelectListItem> stateitems = new List<SelectListItem>();

            stateitems.Add(new SelectListItem { Text = "Arizona", Value = "1" });

            stateitems.Add(new SelectListItem { Text = "California", Value = "2" });

            stateitems.Add(new SelectListItem { Text = "Colorado", Value = "3" });

            ViewBag.StateName = stateitems.ToList();
        }



        public void CountryDropDownList()
        {
            List<SelectListItem> countryitems = new List<SelectListItem>();

            countryitems.Add(new SelectListItem { Text = "Pakistan", Value = "Pak" });

            countryitems.Add(new SelectListItem { Text = "UK", Value = "Uk" });

            countryitems.Add(new SelectListItem { Text = "USA", Value = "USA" });

            ViewBag.CountryName = countryitems.ToList();
        }
        // GET: Demo
        public ActionResult Index()
        {
            StateDropDownList();
            CountryDropDownList();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StateName(string StateName , string CountryName)
        {
            ViewBag.ResultMessage = "Sate Name "+ StateName + "Country Name" + CountryName;

            using (Connection db = new Connection())
            {
                List<Rating> rt = new List<Rating>();

                rt.Add(new Rating
                {
                    Value = StateName
                });

                rt.Add(new Rating
                {
                    Value = CountryName
                });

                foreach (var item in rt)
                {
                    db.Ratings.Add(item);
                }
                
                db.SaveChanges();
            }
                StateDropDownList();
            CountryDropDownList();
            return View("Index");
        }
    }
}