﻿
using Scoutpocket.Models;
using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.Models.Repository;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Scoutpocket.Attributes;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;

namespace scoutpocket.Controllers
{
    [SessionExpireFilter]
    public class PlayerController : Controller
    {
        private Connection db = new Connection();

        IPlayerRepository _repository;

        public PlayerController() : this(new PlayerRepository())
        {

        }

        public PlayerController(IPlayerRepository repository)
        {
            _repository = repository;
        }

        public JsonResult IsUserExists(string UserMail)
        {
            return Json(!db.Users.Any(x => x.UserMail == UserMail), JsonRequestBehavior.AllowGet);
        }

  

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(MyModel model)
        {
            string imagePath = Server.MapPath("~/" + model.CroppedImagePath);

            byte[] imageBytes = System.IO.File.ReadAllBytes(imagePath);

            //Save file whatever you want
            // "PlayerProfile"
            return View("HomeController");
        }


        [Authorize(Roles = "2")]
        [Route("spiller-profil/{id?}")]
        public ActionResult GetPlayerByID(int id = 0)
        {
            var getPlayerByID = _repository.GetPlayerByID(id);

            User user = new User();
            Connection connection = new Connection();

            user = connection.Users.SingleOrDefault(x => x.profile.player.PlayerID == id);


           
            int PlayerID = user.profile.player.PlayerID;
            ViewBag.ClubListID = new SelectList(db.ClubLists, "ClubListID", "ClubName");

            int ClubID = Convert.ToInt32(System.Web.HttpContext.Current.Session["GetClubID"]);
                getPlayerByID.isOwningClub = ClubID == getPlayerByID.player.ClubListID;

            DateTime Time = DateTime.Now;
            CreateView(PlayerID, ClubID, Time);
            //ShowPlayerData(id);

            TempData["GetPlayerID"] = user.profile.player.PlayerID;





            ShowPlayerDataByID();

            TempData["ID"] = id;

           

            return View(getPlayerByID);
        }

        [HttpPost]
        public ActionResult CreateView(int PlayerID, int ClubID, DateTime Time)
        {
            var Views = new View
            {
                PlayerID = PlayerID,
                ClubID = ClubID,
                Time = Time

            };

            db.Views.Add(Views);

            db.SaveChanges();

            return View();
        }

        [HttpPost]
        public ActionResult UpdatePlayer(int clubID, int seriesID,int regionID)
        {
            
            if (User.Identity.IsAuthenticated)
            {

                User user = new User();
                Connection connection = new Connection();
                
                var GetUserName = User.Identity.Name;
                //TODO: Make it more secure by not using a getusername
                user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);
                if (clubID > 0)
                {
                    user.profile.player.ClubListID = clubID;
                }
                if (seriesID > 0)
                {
                    user.profile.player.SeriesID = seriesID;
                }
                if (regionID > 0)
                {
                    //this gives error
                    Region newregion = connection.Regions.SingleOrDefault(r => r.RegionID == regionID);
                    user.profile.player.Region = newregion;
                }
                connection.SaveChanges();
                connection.Dispose();
            }

            //There is an error in firefox where it says XML Parsing Error: no element found
            //the only way to solve this (I think) is by return a view with parameters causing the site to reload.
            //but it is not supposed to reload the page...
            return View();
        }

        public ActionResult GetViewGroupBy()
        {
            int id = Convert.ToInt32(TempData["ID"]);

            var clubs = db.ClubLists.ToList();

            var views = db.Views.OrderByDescending(i => i.ViewID).ToList();



            var query = (from Views in views
                         join ClubList in clubs on Views.ClubID equals ClubList.ClubListID
                         where Views.PlayerID == id

                         select new ViewModel
                         {


                             ClubName = ClubList.ClubName,
                             ClubImage = ClubList.ClubImage,
                             Time = Views.Time

                         });



            return View(query.GroupBy(x => x.ClubName, (key, c) => c.FirstOrDefault()));
        }

        [Authorize(Roles = "3")]
        [Route("spiller-profil")]
        public ActionResult PlayerProfile()
        {

            if (User.Identity.IsAuthenticated)
            {
                User user = new User();
                Connection connection = new Connection();
                var GetUserName = User.Identity.Name;
                user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);
                int userID = user.UserID;

                ShowPlayerData(userID);

                //ShowPlayerDataByID();
                TempData["ID"] = userID;

                List<Series> dbSeries = db.Seriess.Include(y => y.SerieCategories).ToList();
                SerieCategory seriesCategory = new SerieCategory();


                foreach (Series series in dbSeries)
                {
                    if (series.SeriesID == user.profile.player.Series.SeriesID)
                    {

                        seriesCategory = series.SerieCategories.Single();
                        break;
                    }
                }
                JsonResult jsonResult = GetSeries(seriesCategory.SerieCategoryID, user.profile.player.Region.RegionID);

                System.Web.Mvc.SelectList test = (System.Web.Mvc.SelectList)jsonResult.Data;

                               

                //var JSONSeries = GetSeries(seriesCategory.SerieCategoryID, user.profile.player.Region.RegionID).Data;

                //var currentSeriesCategoryId = user.profile.player.series.Region.SerieCategoryID;

                ViewBag.Series = test;

                //this seems to be used for the edit series category and clublist and region button for the players and clubs 
                ViewBag.SeriesCategory = new SelectList(db.SerieCategories.Where(c => c.GenderID == seriesCategory.GenderID).ToList(), "SerieCategoryID", "SerieCategoryName");
                ViewBag.newSeriesCategories = new SelectList(db.SerieCategories.Where(c => c.GenderID == seriesCategory.GenderID).ToList(), "SerieCategoryID", "SerieCategoryName");
                ViewBag.ClubListID = new SelectList(db.ClubLists, "ClubListID", "ClubName");
                ViewBag.Region = new SelectList(db.Regions, "RegionID", "RegionName", "RegionID");
                //ViewBag.Series = new SelectList(JSONSeries, "SeriesID", "SeriesName");
                
            }


            return View();

            // return RedirectToAction("getRate");
        }



        public ActionResult getRate(ProfileViewModel model)
        {
            GetAllRatingModels(model.ProfileID);
            return View();
            //return RedirectToAction("PlayerProfile");
        }

        public ActionResult GetPropertiesAVG()
        {
            PlayerProfile();

            return View();
        }

        public ActionResult GetPropertiesAVGByID()
        {
            GetPlayerByID();

            return View();
        }



        public ActionResult getNameAndClub()
        {
            int getUserID;
            var GetUserName = User.Identity.Name;
            User user = db.Users.SingleOrDefault(a => a.UserMail == GetUserName);

            getUserID = user.UserID;
            return View(db.Profiles.SingleOrDefault(x => x.ProfileID == getUserID));
        }
        private User GetCurrentUser()
        {
            User user = new User();
            Connection connection = new Connection();
            var GetUserName = User.Identity.Name;
            user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);
            return user;
        }
        public ActionResult SaveImage()
        {

            if (User.Identity.IsAuthenticated)
            {
                User user = new User();
                Connection connection = new Connection();
                var GetUserName = User.Identity.Name;
                user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);
                int id = user.UserID;

                var profile = connection.Profiles.Where(x => x.ProfileID == id).SingleOrDefault();

                var getFileName = Session["getFileName"];

                if (getFileName != null)
                {
                    profile.Image = getFileName.ToString();
                }



                connection.SaveChanges();


            }

            return RedirectToAction("PlayerProfile");

        }



        public ActionResult GetPosition()
        {

            int getUserID;
            var GetUserName = User.Identity.Name;
            User user = db.Users.SingleOrDefault(a => a.UserMail == GetUserName);

            getUserID = user.UserID;
            return View(db.Profiles.SingleOrDefault(x => x.ProfileID == getUserID));
        }



        public IQueryable<Profile> getProfiles()
        {

            return db.Profiles;

        }


        public List<Properties> getProperties()
        {
            return db.Propertiess.ToList();
        }

        public List<SubProperties> getSubproperties()
        {
            return db.SubPropertiess.ToList();
        }

        public List<Rating> getRatings()
        {
            return db.Ratings.ToList();
        }



        public ActionResult GetAllRatingModels(int? profileId = null)
        {

            RatingViewModel rv = new RatingViewModel();

            if (profileId != null) {
                rv.user = db.Profiles.First(p => p.ProfileID == profileId.Value).user;
            }
            rv.CurrentUser = db.Profiles.Include("club").First(p => p.user.UserMail == User.Identity.Name);
            // get list of all the profies
            
            rv.profile = getProfiles().Where(p => p.ProfileID == profileId.Value);
            // get list of all the properties
            rv.properties = getProperties();
            //get list of subproperties
            rv.subProperties = getSubproperties();
            rv.ProfileID = profileId.Value;
            //get list of ratnings
            var playerId = rv.profile.First().player.PlayerID;
            rv.player = db.Players.First(p => p.PlayerID == playerId);
            rv.rating = getRatings().Where(p => p.PlayerID == playerId);

            var comments = db.PlayerPropertyScoutComment.Where(p => p.PlayerID == playerId).OrderByDescending(c => c.Created).ToList();
            rv.Comments = comments;

            return View(rv);
        }



        public JsonResult CheckUserName(string UserMail)
        {
            Connection db = new Connection();
            var result = true;
            var user = db.Users.Where(x => x.UserMail == UserMail).FirstOrDefault();

            if (user != null)
                result = false;

            return Json(result, JsonRequestBehavior.AllowGet);
        }




        [Route("opret-spiller")]
        public ActionResult CreatePlayer()
        {


            ViewBag.ClubListID = new SelectList(db.ClubLists, "ClubListID", "ClubName");
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "PositionName", "PositionCategory", 0);
            ViewBag.Region = new SelectList(db.Regions, "RegionID", "RegionName");


            var gender = db.Genders.ToList();

            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text= "Vælg køn", Value = "0" });

            foreach (var item in gender)
            {
                li.Add(new SelectListItem { Text = item.GenderType, Value = item.GenderID.ToString() });
            }

            ViewBag.gender = li;




            return View();
        }

        [Route("opret-spiller")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreatePlayer(PlayerProfileViewModel viewModel, Player player)
        {
            ViewBag.ClubListID = new SelectList(db.ClubLists, "ClubListID", "ClubName", player.ClubListID).SelectedValue;
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "PositionName", "PositionCategory", player.PositionID).SelectedValue;
            ViewBag.SeriesID = new SelectList(db.Seriess).SelectedValue;
            if (Session["SeriesID"] != null)
            {
                player.SeriesID = (int)Session["SeriesID"];
            }
            
            Session["viewModel"] = viewModel;
            Session["player"] = player;

            return RedirectToAction("ChoosePlayerAbonnement");

        }


        public JsonResult GetSerieCategories(int id)
        {
            var seriesCat = db.SerieCategories.Where(x => x.GenderID == id).ToList();
            List<SelectListItem> liseriesCat = new List<SelectListItem>();

            liseriesCat.Add(new SelectListItem { Text = "Vælg serie kategori", Value = "0" });

            if (seriesCat != null)
            {
                foreach (var x in seriesCat)
                {
                    liseriesCat.Add(new SelectListItem { Text = x.SerieCategoryName, Value = x.SerieCategoryID.ToString() });
                }
            }



            return Json(new SelectList(liseriesCat, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        [HttpPost]
        public JsonResult GetSeriesID(int id)
        {
            
            //I do not know what this is really
            Session["SeriesID"] = id;

            var series = db.Seriess.Where(x => x.SeriesID == id).ToList();
            List<SelectListItem> liseries = new List<SelectListItem>();

            liseries.Add(new SelectListItem { Text = "Vælg serie", Value = "0" });
            if (series != null)
            {
                foreach (var l in series)
                {
                    liseries.Add(new SelectListItem { Text = l.SeriesName, Value = l.SeriesID.ToString() });

                }
            }



            return Json(new SelectList(liseries, "Value", "Text", JsonRequestBehavior.AllowGet));
        }


        public JsonResult GetRegions()
        {
            List<Region> regions = db.Regions.ToList();

            List<SelectListItem> liseries = new List<SelectListItem>();

            liseries.Add(new SelectListItem { Text = "Vælg landsdel", Value = "0" });
            if (regions != null)
            {
                foreach (var l in regions)
                {
                    liseries.Add(new SelectListItem { Text = l.RegionName, Value = l.RegionID.ToString() });
                }
            }

            return Json(new SelectList(liseries, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        public JsonResult GetSeries(int seriesCategoryID, int regionID)
        {

            
            List<Series> dbSeries = db.Seriess.Include(x => x.Regions).Include(y => y.SerieCategories).ToList();

            //the series that meets the conditions for seriesCategory and region
            List<Series> relatedSeries = new List<Series>();

            foreach (Series series in dbSeries)
            {
                
                foreach(Region region in series.Regions)
                {
                    if(region.RegionID == regionID)
                    {
                        foreach(SerieCategory seriesCategory in series.SerieCategories)
                        {
                            if(seriesCategory.SerieCategoryID == seriesCategoryID)
                            {
                                relatedSeries.Add(series);
                            }
                        }
                    }
                }
            }          


            List<SelectListItem> liseries = new List<SelectListItem>();

            liseries.Add(new SelectListItem { Text = "Vælg serie", Value = "0" });
            if (relatedSeries != null)
            {
                foreach (var l in relatedSeries)
                {
                    liseries.Add(new SelectListItem { Text = l.SeriesName, Value = l.SeriesID.ToString() });


                }
            }

            return Json(new SelectList(liseries, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        [Authorize(Roles = "3")]
        [Route("aendre-abonnement")]
        public ActionResult ChangePlayerSubscription()
        {
            ViewBag.SeriesID_Man = Session["SeriesMAN"];
            ViewBag.player = Session["player"] as Player;
            ViewBag.viewModel = Session["viewModel"] as PlayerProfileViewModel;


            string dateCode = GetDateCode(DateTime.Now);
            int priceBasis = 0;
            int pricePhysic = 0;
            int priceMental = 0;
            int priceBoth = 0;

            int.TryParse(WebConfigurationManager.AppSettings["priceBasis"], out priceBasis);
            int.TryParse(WebConfigurationManager.AppSettings["pricePhysic" + dateCode], out pricePhysic);
            int.TryParse(WebConfigurationManager.AppSettings["priceMental" + dateCode], out priceMental);
            int.TryParse(WebConfigurationManager.AppSettings["priceBoth" + dateCode], out priceBoth);

            ViewBag.Basis = priceBasis;
            ViewBag.Physical = pricePhysic;
            ViewBag.Mental = priceMental;
            ViewBag.Both = priceBoth;

            ViewBag.ExistingCategory = 0;

            if (User.Identity.IsAuthenticated)
            {
                int userId = db.Users.SingleOrDefault(x => x.UserMail == User.Identity.Name).profile.ProfileID;
                //try and find the preparedsubscription first. If not found then use the subscription category
                int category = 0;
                try
                {
                    category = db.PreparedSubscriptions.SingleOrDefault(x => x.PrepId == userId).Category;
                }
                catch (Exception)
                {
                    category = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == userId).Category;
                }
                //Since the user changes the subscriptions, then it is better to see the existingcategory by the one he has chosen in preparesubscription. If he changes mind again.
                ViewBag.ExistingCategory = category;
            }
            
            return View();
        }

        [Route("spiller-abonnement")]
        public ActionResult ChoosePlayerAbonnement()
        {
            ViewBag.SeriesID_Man = Session["SeriesMAN"];
            ViewBag.player = Session["player"] as Player;
            ViewBag.viewModel = Session["viewModel"] as PlayerProfileViewModel;
            

            string dateCode = GetDateCode(DateTime.Now);
            int priceBasis = 0;
            int pricePhysic = 0;
            int priceMental = 0;
            int priceBoth = 0;

            int.TryParse(WebConfigurationManager.AppSettings["priceBasis"], out priceBasis);
            int.TryParse(WebConfigurationManager.AppSettings["pricePhysic" + dateCode], out pricePhysic);
            int.TryParse(WebConfigurationManager.AppSettings["priceMental" + dateCode], out priceMental);
            int.TryParse(WebConfigurationManager.AppSettings["priceBoth" + dateCode], out priceBoth);

            ViewBag.Basis = priceBasis;
            ViewBag.Physical = pricePhysic;
            ViewBag.Mental = priceMental;
            ViewBag.Both = priceBoth;

            ViewBag.ExistingCategory = 0;

            return View();
        }


        /// <summary>
        /// Action Result that handles the choosing of a subscription. This accords for both creating new profiles,
        /// and updating existing profiles. Routes to either a payment window, or playerprofile on updates.
        /// </summary>
        /// <param name="SubscriptionChoice"></param>
        /// <returns></returns>
        [Route("playersubscriptionchoice/{SubscriptionChoice?}")]
        public ActionResult PlayerSubscriptionChoosen(int SubscriptionChoice = 0)
        {
            //Terminate if choice isn't 1 - 4
            if (SubscriptionChoice < 1 || SubscriptionChoice > 4)
            {
                return View(); //Displays an error message
            }

            //Get configuration
            string merchantNumber = WebConfigurationManager.AppSettings["apiMerchantNumber"] as string;
            string currency = WebConfigurationManager.AppSettings["apiCurrency"] as string;
            string acceptURL = HttpContext.Request.Url.Authority + "/epay-callback"; //ttp://scoutpocket.dk/epay-callback";
            int amount;
            bool priceConverted = int.TryParse(GetPriceForChoosenCategory(SubscriptionChoice, DateTime.Now), out amount);


            //Result for a logged in user
            if (User.Identity.IsAuthenticated)
            {
                User user = db.Users.SingleOrDefault(a => a.UserMail == User.Identity.Name);
                Subscriptions userSub = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == user.UserID);

                //If user goes from Basis to Other, goto Payment window
                if (1 < SubscriptionChoice && userSub.Category != 0 && userSub.Category == 1)
                {
                    Session["UpdatingSubscriptionId"] = userSub.SubscriptionID;
                    goto OpenPaymentWindow;
                }
                //If user goes from Other to Basis
                if (SubscriptionChoice == 1)
                {
                    _repository.CreateORUpdatePreparedSubscription(user.UserID, amount, true, 1);
                    return RedirectToAction("PlayerProfile");
                }
                //If user goes from Other to Other
                if (1 < SubscriptionChoice)
                {
                    _repository.CreateORUpdatePreparedSubscription(user.UserID, amount, true, SubscriptionChoice);
                    return RedirectToAction("PlayerProfile");
                }

                return RedirectToAction("PlayerProfile");
            }


            //Result if a user is being created
            if (Session["player"] != null)
            {
                //Basis shouldn't open a payment window, so it gets intercepted
                if (SubscriptionChoice == 1)
                {
                    PlayerProfileViewModel viewModel = Session["viewModel"] as PlayerProfileViewModel;
                    Player player = Session["player"] as Player;
                    int regionID = viewModel.RegionID;
                    Region region = db.Regions.Where(x => x.RegionID == regionID).Single();
                    player.Region = region;
                    _repository.CreateNonPayingPlayer(viewModel, player, SubscriptionChoice, region); //TODO mailsystem?
                    Session["Product"] = "Basis"; //it did not saved it properly before, now it should do it -hamun
                    //works here
                    SendMail();

                    //autologin
                    FormsAuthentication.SetAuthCookie(viewModel.UserMail, false);

                    return RedirectToAction("PlayerProfile");
                }

                goto OpenPaymentWindow;
            }

            return RedirectToAction("PlayerProfile");

            OpenPaymentWindow:

            //Make these for use in callback
            Session["CreationPriceSent"] = amount;
            Session["CreationSelectionPicked"] = SubscriptionChoice;

            //Amount sent is 0, actual payments get made via the api at the end of month 
            string hashsource = merchantNumber + "0" + currency + "3" + "1" + acceptURL + WebConfigurationManager.AppSettings["apiHashCode"];

            string redirectParams = string.Format("merchantnumber={0}&amount={1}&currency={2}&windowstate={3}&subscription={4}&accepturl={5}&hash={6}",
                merchantNumber, "0", currency, "3", "1", acceptURL, GetMD5hash(hashsource));

            return Redirect(@"https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx?" + redirectParams);
        }

        public ActionResult SaveProductInSession(string product)
        {
            Session["Product"] = product;
            return Json(new { Status = "Success" });
        }

        [Route("epay-callback")]
        public ActionResult CallbackEpay()
        {
            //Get HTTP from epay
            int subscriptionID; // the subscriptionid from epay or subscriptionIDEpay in subscriptions table in db
            int price = 0;
            int transactionID;
            int subCategory = 0;

            price = (int)Session["CreationPriceSent"];
            subCategory = (int)Session["CreationSelectionPicked"];

            string subscriptionIDstring = this.Request.QueryString["subscriptionid"];
            Int32.TryParse(subscriptionIDstring, out subscriptionID);

            // NB this gets returned as a string with 00 behind it (100 kr -> 10000kr)
            //string priceString = this.Request.QueryString["amount"];
            //Int32.TryParse(priceString, out price);

            string transactionIDString = this.Request.QueryString["txnid"];
            Int32.TryParse(transactionIDString, out transactionID);

            string orderIDString = this.Request.QueryString["orderid"];


            //If updating an existing subscription, intercept the normal creation
            if (User.Identity.IsAuthenticated)
            {
                int targetId = 0;
                targetId = (int)Session["UpdatingSubscriptionId"];
                _repository.CreateORUpdatePreparedSubscription(targetId, subscriptionID, price, transactionID, orderIDString, true, subCategory);
                SendMail();
                return RedirectToAction("PlayerProfile");
            }


            //Normal Creation of new profile

            string product = Session["Product"] as string;

            PlayerProfileViewModel viewModel = Session["viewModel"] as PlayerProfileViewModel;
            Player player = Session["player"] as Player;
            int regionID = viewModel.RegionID;
            Region region = db.Regions.Where(x => x.RegionID == regionID).Single();
            player.Region = region;
            // If creation is outside creation time, put the subscription on hold
            if (IsCreationMonth(DateTime.Now))
            {
                

                _repository.CreatePlayer(viewModel, player, subscriptionID, price, transactionID, orderIDString, product, true, subCategory, region);
                SendMail();
            }
            else
            {
                _repository.CreatePlayer(viewModel, player, subscriptionID, price, transactionID, orderIDString, product, false, subCategory, region);
                SendMail();
            }

            //autologin?
            FormsAuthentication.SetAuthCookie(viewModel.UserMail, false);
            
            return RedirectToAction("PlayerProfile");
            
        }


        public ActionResult SendMail()
        {

            //NOTICE, when you change in the database the session might make problems. There make sure that this is correct.
            PlayerProfileViewModel viewModel = Session["viewModel"] as PlayerProfileViewModel;
            Player player = Session["player"] as Player;

            string getProduct = Session["Product"].ToString();


            SmtpClient smtpClient = new SmtpClient();


            //from https://www.lifewire.com/g00/what-are-the-outlook-com-smtp-server-settings-1170671?i10c.referrer=https%3A%2F%2Fwww.google.dk%2F
            //and https://support.office.com/en-us/article/Add-your-Outlook-com-account-to-another-mail-app-73f3b178-0009-41ae-aab1-87b80fa94970
            /*smtpClient.Host = "smtp-mail.outlook.com";
            smtpClient.Credentials = new System.Net.NetworkCredential("scoutpocket@outlook.dk", "");
            //this only works if the url is https... So for local testing do regular.
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.Port = 587;
            */
            if (getProduct == "Basis")
            {

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Mails/Scout-pocket-basis.html")))
                {


                    MailMessage mailMessage = new MailMessage();

                    MailAddress fromAddress = new MailAddress("scoutpocket@outlook.dk");
                    
                    //scoutpocket @outlook.dk

                    mailMessage.From = fromAddress;

                    mailMessage.To.Add(viewModel.UserMail);


                    mailMessage.IsBodyHtml = true;
                    mailMessage.Subject = "Velkommen til Scout Pocket";



                    mailMessage.Body = reader.ReadToEnd();



                    smtpClient.Send(mailMessage);
                    //remember to close the connection
                    smtpClient.Dispose();
                }

            }


            if (getProduct == "Fysisk")
            {

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Mails/Scout-pocket-fysisk.html")))
                {


                    MailMessage mailMessage = new MailMessage();

                    MailAddress fromAddress = new MailAddress("scoutpocket@outlook.dk");

                    mailMessage.From = fromAddress;

                    mailMessage.To.Add(viewModel.UserMail);


                    mailMessage.IsBodyHtml = true;
                    mailMessage.Subject = "Velkommen til Scout Pocket";





                    mailMessage.Body = reader.ReadToEnd();


                  

                    smtpClient.Send(mailMessage);
                    //remember to close the connection
                    smtpClient.Dispose();
                }

            }

            if (getProduct == "Mental")
            {

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Mails/Scout-pocket-mental.html")))
                {


                    MailMessage mailMessage = new MailMessage();

                    MailAddress fromAddress = new MailAddress("scoutpocket@outlook.dk");

                    mailMessage.From = fromAddress;

                    mailMessage.To.Add(viewModel.UserMail);


                    mailMessage.IsBodyHtml = true;
                    mailMessage.Subject = "Velkommen til Scout Pocket";





                    mailMessage.Body = reader.ReadToEnd();

                    smtpClient.Send(mailMessage);
                    //remember to close the connection
                    smtpClient.Dispose();
                }

            }


            if (getProduct == "MentalFysisk")
            {


                using (StreamReader reader = new StreamReader(Server.MapPath("~/Mails/Scout-pocket-fysisk-mental.html")))
                {


                    MailMessage mailMessage = new MailMessage();

                    MailAddress fromAddress = new MailAddress("scoutpocket@outlook.dk");

                    mailMessage.From = fromAddress;

                    mailMessage.To.Add(viewModel.UserMail);


                    mailMessage.IsBodyHtml = true;
                    mailMessage.Subject = "Velkommen til Scout Pocket";


                    mailMessage.Body = reader.ReadToEnd();


                    smtpClient.Send(mailMessage);
                    //remember to close the connection
                    smtpClient.Dispose();
                }
            }



            return View();
        }



        //virker ikke med void
        public ActionResult ShowPlayerDataByID()
        {
            int getPlayerID = Convert.ToInt32(TempData["GetPlayerID"]);

            Player user = new Player();
            Connection connection = new Connection();

            user = connection.Users.Where(x => x.profile.player.PlayerID == getPlayerID).SingleOrDefault().profile.player;
            var player = connection.Profiles.Where(p => p.player.PlayerID == getPlayerID).First().player;
            var seriesId = player.SeriesID;
            var positionId = player.PositionID;
            var peopleInSameSeriesPosition = connection.Profiles.Where(x => x.player.PositionID == positionId && x.player.SeriesID == seriesId).SelectMany(p => p.player.Ratings).ToList();
            
            //     ViewBag.Test = user.profile.LastName;

            List<Properties> properties = connection.Propertiess.ToList();

            int[] sumProps = new int[3]; //this is ONLY used for the radarchart
            double[] percentProps = new double[3];

            //the below is used for average circles for the properties
            double[] avgProp = { 0.0, 0.0, 0.0 };
           
            int i = 0;
            List<object[]> objectProp = new List<object[]>();
            List<object[]> oAvgProp = new List<object[]>();
            foreach (var prop in properties)
            {

                //the different properties teknik etc.
                var subProps = prop.SubProperties.Where(x => x.PositionID == user.PositionID);

                if (subProps.Count() > 0)
                {


                    foreach (var subProp in subProps)
                    {
                       
                        //used for the radarchart. Finds the sum of all the ratings for each subprop
                        var ratings = subProp.rating.Where(x => x.SubPropertiesID == subProp.SubPropertiesID && x.PlayerID == user.PlayerID).OrderByDescending(x => x.Date);
                        if (ratings.Count() > 0)
                        {

                            int lastValue;
                            //used for the number in the circlenumber get the last rating and sum them all.
                            Rating lastRating = subProp.rating.Where(x => x.PlayerID == getPlayerID).ToList().OrderBy(x => x.Date).Last();
                            Int32.TryParse(lastRating.Value, out lastValue);
                            avgProp[i] += lastValue;
                            foreach (Rating rating in ratings)
                            {
                                int value = 0;
                                Int32.TryParse(rating.Value, out value);
                                //Step 2:
                                sumProps[i] += value;


                            }

                        }


                    }
                    if (avgProp[i] > 0.0)
                    {

                        avgProp[i] = Math.Round((double)avgProp[i] / subProps.Count(), 2);
                    }



                    object[] subObjectProp = new object[subProps.Count() * 2];
                    object[] OsubObjectProp = new object[subProps.Count() * 2];
                    int subPropIndex = 0;
                    foreach (var subProp in subProps)
                    {
                        //used for the new radarchart (which is like x coordinates and y coordinates seperated)
                        //TODO?
                      
                        OsubObjectProp[subPropIndex] = subProp.SubPropertiesName;
                        
                        //used for the old radarchart
                        subObjectProp[subPropIndex] = subProp.SubPropertiesName;
                        subPropIndex++;

                        var ratings = subProp.rating.Where(x => x.PlayerID == getPlayerID).OrderByDescending(x => x.Date).Take(1);
                        if (ratings.Count() > 0)
                        {
                            var ratingAverageForProp = peopleInSameSeriesPosition.Where(p => p.SubPropertiesID == subProp.SubPropertiesID).Select(p =>
                             {
                                 int aValue = 0;
                                 int.TryParse(p.Value, out aValue);
                                 return aValue;
                             });

                            var propAverage = ratingAverageForProp.Average();
                            propAverage = Math.Round(propAverage, 1);
                            OsubObjectProp[subPropIndex] = propAverage;

                            //Save percentages for the radar chart
                            int value = -1;
                            Int32.TryParse(ratings.First().Value, out value);
                            percentProps[i] = value;
                            //percentProps[i] = (double)value;
                            //step 3: the radarchart array: subpropname = axis, percentProps = value
                            subObjectProp[subPropIndex] = percentProps[i];
                            subPropIndex++;
                        }
                    }
                    objectProp.Add(subObjectProp);
                    oAvgProp.Add(OsubObjectProp);
                    i++;

                }
                ViewBag.avgProp = avgProp;
                ViewBag.radarObject = objectProp;
                ViewBag.OtherPlayerAverage = oAvgProp;
            }





                return View();



        }


        public void ShowPlayerData(int id)
        {


            Player user = new Player();
            Connection connection = new Connection();

            user = connection.Users.Where(x => x.profile.player.PlayerID == id).SingleOrDefault().profile.player;
            var player = connection.Profiles.Where(p => p.player.PlayerID == id).First().player;
            var seriesId = player.SeriesID;
            var positionId = player.PositionID;
            var peopleInSameSeriesPosition = connection.Profiles.Where(x => x.player.PositionID == positionId && x.player.SeriesID == seriesId).SelectMany(p => p.player.Ratings).ToList();

          

            if (!string.IsNullOrEmpty(Session["getProfileID"] as string))
            {
                Session["getProfileID"] = user.Profile.player.PlayerID;
            }

            else
            {
                Session["getProfileID"] = user.Profile.player.PlayerID;
            }



            Session["getPlayerPositionID"] = user.Profile.player.PlayerID;
            /* This is used for finding the avg of the newest scout rating */
            List<Properties> properties = connection.Propertiess.ToList();

            int[] sumProps = new int[3]; //this is ONLY used for the radarchart
            double[] percentProps = new double[3];

            //the below is used for average circles for the properties
            double[] avgProp = { 0.0, 0.0, 0.0 };

            int i = 0;
            List<object[]> objectProp = new List<object[]>();
            List<object[]> oAvgProp = new List<object[]>();
            foreach (var prop in properties)
            {

                //the different properties teknik etc.
                var subProps = prop.SubProperties.Where(x => x.PositionID == user.PositionID);
                if (subProps.Count() > 0)
                {


                    foreach (var subProp in subProps)
                    {


                        //used for the radarchart. Finds the sum of all the ratings for each subprop
                        var ratings = subProp.rating.Where(x => x.SubPropertiesID == subProp.SubPropertiesID && x.PlayerID == user.PlayerID).OrderByDescending(x => x.Date);
                        if (ratings.Count() > 0)
                        {

                            int lastValue;
                            //used for the number in the circlenumber get the last rating and sum them all.
                            Rating lastRating = subProp.rating.Where(x => x.PlayerID == id).ToList().OrderBy(x => x.Date).Last();
                            Int32.TryParse(lastRating.Value, out lastValue);
                            avgProp[i] += lastValue;
                            foreach (Rating rating in ratings)
                            {
                                int value = 0;
                                Int32.TryParse(rating.Value, out value);
                                //Step 2:
                                sumProps[i] += value;


                            }

                        }


                    }
                    if (avgProp[i] > 0.0)
                    {

                        avgProp[i] = Math.Round((double)avgProp[i] / subProps.Count(), 2);
                    }



                    object[] subObjectProp = new object[subProps.Count() * 2];
                    object[] OsubObjectProp = new object[subProps.Count() * 2];
                    int subPropIndex = 0;
                    foreach (var subProp in subProps)
                    {
                        //used for the new radarchart (which is like x coordinates and y coordinates seperated)
                        //TODO?
                     
                        OsubObjectProp[subPropIndex] = subProp.SubPropertiesName;
                        //used for the old radarchart
                        subObjectProp[subPropIndex] = subProp.SubPropertiesName;
                        subPropIndex++;
                        var ratings = subProp.rating.Where(x => x.PlayerID == id).OrderByDescending(x => x.Date).Take(1);
                        if (ratings.Count() > 0)
                        {
                            var ratingAverageForProp = peopleInSameSeriesPosition.Where(p => p.SubPropertiesID == subProp.SubPropertiesID).Select(p =>
                            {
                                int aValue = 0;
                                int.TryParse(p.Value, out aValue);
                                return aValue;
                            });

                            var propAverage = ratingAverageForProp.Average();
                            propAverage = Math.Round(propAverage, 1);
                            OsubObjectProp[subPropIndex] = propAverage;
                            //Save percentages for the radar chart
                            int value = -1;
                            Int32.TryParse(ratings.First().Value, out value);
                            percentProps[i] = value;
                            //percentProps[i] = (double)value;
                            //step 3: the radarchart array: subpropname = axis, percentProps = value
                            subObjectProp[subPropIndex] = percentProps[i];
                            subPropIndex++;
                        }
                    }
                    objectProp.Add(subObjectProp);
                    oAvgProp.Add(OsubObjectProp);
                    i++;

                }
                ViewBag.avgProp = avgProp;

                //step 4:
                //var json = JsonConvert.SerializeObject(objectProp);


                ViewBag.radarObject = objectProp;
                ViewBag.OtherPlayerAverage = oAvgProp;
                //profile.Image = getFileName.ToString();

                //connection.SaveChanges();
            }
            /* This is used to create the radar chart 
         * Step 1. find the sum of the newest subprop values/numbers together = sumProps array
         * Step 2. divide each subproperty with the total (the percent)
         * Step 3. Make an array looking like[//iPhone
                    {axis:"Battery Life",value:15},
                    {axis:"Brand",value:12},
                    {axis:"Contract Cost",value:20},
                    {axis:"Design And Quality",value:16},
                    {axis:"Have Internet Connectivity",value:14},
                    {axis:"Large Screen",value:10},
                    {axis:"Price Of Device",value:9},
                    {axis:"To Be A Smartphone",value:8}
                  ]
            Step 4. Save it. And that is used for the radar chart*/



            //remove to playerProfile
            getNameAndClub();

            GetPosition();
        }

    

        // Utility

        /// <summary>
        /// Get price from appsettings in Web.config
        /// </summary>
        /// <param name="choice">1=Basis, 2=Fysisk, 3=Mental, 4=Begge</param>
        /// <param name="now">This should be current time at call, DateTime.now</param>
        /// <returns></returns>
        private string GetPriceForChoosenCategory(int choice, DateTime now)
        {
            string amount;
            string datecode = GetDateCode(now);

            switch (choice)
            {
                case 2:
                    amount = WebConfigurationManager.AppSettings["pricePhysic" + datecode] as string;
                    break;
                case 3:
                    amount = WebConfigurationManager.AppSettings["priceMental" + datecode] as string;
                    break;
                case 4:
                    amount = WebConfigurationManager.AppSettings["priceBoth" + datecode] as string;
                    break;
                default:
                    amount = WebConfigurationManager.AppSettings["priceBasis"] as string;
                    break;
            }

            return amount;
        }

        /// <summary>
        /// Get a month in text, based on current month
        /// </summary>
        /// <param name="now">DateTime.now</param>
        /// <returns></returns>
        private string GetDateCode(DateTime now)
        {
            string code = "January";
            switch (now.Month)
            {
                case 9:
                case 10:
                case 11:
                case 12:
                case 1:
                    code = "January";
                    break;
                case 2:
                    code = "February";
                    break;
                case 3:
                    code = "March";
                    break;
                case 4:
                case 5:
                case 6:
                case 7:
                    code = "July";
                    break;
                case 8:
                    code = "August";
                    break;
                default:
                    break;
            }
            return code;
        }

        /// <summary>
        /// Supply a time and get if the supplied month qualifies as an active month.
        /// ie. Should a subscription start immediatly
        /// </summary>
        /// <param name="now">DateTime.now</param>
        /// <returns></returns>
        private bool IsCreationMonth(DateTime now)
        {
            int[] creationMonths = new int[] { 1, 2, 3, 7, 8 };
            if (creationMonths.Contains(now.Month))
            {
                return true;
            }
            return false;
        }

        public string GetMD5hash(string source)
        {
            StringBuilder sBuild = new StringBuilder();
            using (var md5hash = MD5.Create())
            {
                byte[] data = md5hash.ComputeHash(Encoding.UTF8.GetBytes(source));


                for (int i = 0; i < data.Length; i++)
                {
                    sBuild.Append(data[i].ToString("x2"));
                }
            }

            return sBuild.ToString();
        }

        //[Route("secrettestingfunc")]
        //public ActionResult TestingFunction()
        //{
        //    string msg = "";


        //    var sub = db.Subscriptions.ToList()[0];

        //    _repository.CreateORUpdatePreparedSubscription(sub.SubscriptionID, 100, true, 2);

        //    //try
        //    //{

        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    msg += e.InnerException;
        //    //}

        //    ViewBag.Msg = msg;
        //    return View();
        //}
    }
}
