﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scoutpocket.Models.Database;

namespace Scoutpocket.Controllers
{
    public class ClubListsController : Controller
    {
        private Connection db = new Connection();

        [Authorize(Roles = "1")]
        public ActionResult Index()
        {
            return View(db.ClubLists.ToList());
        }

        // GET: ClubLists/Details/5
        [Authorize(Roles = "1")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClubList clubList = db.ClubLists.Find(id);
            if (clubList == null)
            {
                return HttpNotFound();
            }
            return View(clubList);
        }


        // GET: ClubLists/Edit/5
        [Authorize(Roles = "1")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClubList clubList = db.ClubLists.Find(id);
            if (clubList == null)
            {
                return HttpNotFound();
            }
            return View(clubList);
        }

        // POST: ClubLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "1")]
        public ActionResult Edit([Bind(Include = "ClubListID, ClubImage, MyFile, CroppedImagePath")] ClubList clubList)
        {

            Connection connection = new Connection();
            //clubList.ClubListID

            var clublist = connection.ClubLists.SingleOrDefault(a => a.ClubListID == clubList.ClubListID);
            var getFileName = Session["getFileName"];

            clublist.ClubImage = clubList.ClubListID.ToString();

            connection.SaveChanges();

            return RedirectToAction("Edit");
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
