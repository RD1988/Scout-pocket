﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scoutpocket.EpaySubscription;
using Scoutpocket.Models.Database;
using System.Web.Configuration;

namespace Scoutpocket.Controllers
{
    public class SubscriptionController : Controller
    {
        private Connection connection = new Connection();

        authorizeRequestBody authBody = new authorizeRequestBody();
        SubscriptionSoapClient soap = new SubscriptionSoapClient("SubscriptionSoap");

        
        public ActionResult DeleteSubscription()
        {
            
            User user = new User();
            if (User.Identity.IsAuthenticated)
            {
                var GetUserName = User.Identity.Name;
                user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);
                
            }
            soap.Open();
            int merchantNumber = 0;
            int.TryParse(WebConfigurationManager.AppSettings["apiMerchantNumber"], out merchantNumber);
            authBody.merchantnumber = merchantNumber;
            authBody.subscriptionid = user.profile.subscription.SubscriptionIDEpay;
            authBody.orderid = user.profile.subscription.OrderID;
            authBody.amount = user.profile.subscription.Price;
            authBody.currency = 208; //= DKK
            authBody.instantcapture = 0;
            authBody.fraud = 0;
            authBody.pwd = "55141d6f-4079-4d19-b8c6-7a5ec36116a5";

            /*
             * Do not run the one below just yet
             * If the user wants to delete the subscription 
             * then make sure that the rule id = 6 = inactive
             * After that you can delete them from epay etc.
             */

            if(user.profile.subscription.Active == true)
            {
                user.profile.subscription.Active = false;
                //bool deleted = soap.deletesubscription(authBody.merchantnumber, authBody.subscriptionid, authBody.pwd, ref authBody.epayresponse);
            }


            //ViewBag.Deleted = deleted;
            soap.Close();
            
            
            connection.SaveChanges();
            return RedirectToAction("Role", "Auth");
        }

        //www.scoutpocket.dk/Subscription/RecurringSubscriptionPayment 
        public ActionResult RecurringSubscriptionPayment()
        {

            List<Subscriptions> allSubs = connection.Subscriptions.Where(subs => subs.profile.user.RoleID == 3).ToList();


            int merchantNumber = 0;
            int.TryParse(WebConfigurationManager.AppSettings["apiMerchantNumber"], out merchantNumber);
            authBody.merchantnumber = merchantNumber;
            authBody.subscriptionid = 0; 
           
            authBody.amount = 0;
            authBody.currency = 208; //= DKK
            authBody.instantcapture = 0;
            authBody.fraud = 0;
			authBody.pwd = "55141d6f-4079-4d19-b8c6-7a5ec36116a5";
            authBody.email = "lynge_91@hotmail.com";
            //kun transaction id
            //http://tech.epay.dk/da/subscription-web-service
            List<bool> authorizations = new List<bool>();
            List<int> epayresponses = new List<int>();
            List<int> pbsresponses = new List<int>();

            foreach (Subscriptions sub in allSubs)
            {
                authBody.subscriptionid = sub.SubscriptionIDEpay;
                authBody.amount = sub.Price;
                authBody.transactionid = sub.TransactionID;
                authBody.orderid = sub.OrderID.ToString();

            


                bool authorized = soap.authorize(authBody.merchantnumber, authBody.subscriptionid, authBody.orderid, authBody.amount, authBody.currency, authBody.instantcapture,
                    authBody.group, authBody.description, authBody.email, authBody.sms, authBody.ipaddress, authBody.pwd, authBody.textonstatement,
                    authBody.customerkey, ref authBody.fraud, ref authBody.transactionid, ref authBody.pbsresponse, ref authBody.epayresponse);

                epayresponses.Add(authBody.epayresponse);
                pbsresponses.Add(authBody.pbsresponse);


                authorizations.Add(authorized);


            }


            ViewBag.success = authorizations;
            ViewBag.epayresponse = epayresponses;
            ViewBag.pbsresponse = pbsresponses;

            soap.Close();
            return RedirectToAction("Index", "Home");
        }


        public ActionResult RecurringClubSubscriptionPayment()
        {

            List<Subscriptions> allSubs = connection.Subscriptions.Where(subs => subs.profile.user.RoleID == 2).ToList();


            int merchantNumber = 0;
            int.TryParse(WebConfigurationManager.AppSettings["apiMerchantNumber"], out merchantNumber);
            authBody.merchantnumber = merchantNumber;
            authBody.subscriptionid = 0;

            authBody.amount = 0;
            authBody.currency = 208; //= DKK
            authBody.instantcapture = 0;
            authBody.fraud = 0;
            authBody.pwd = "55141d6f-4079-4d19-b8c6-7a5ec36116a5";
            authBody.email = "lynge_91@hotmail.com";
            //kun transaction id
            //http://tech.epay.dk/da/subscription-web-service
            List<bool> authorizations = new List<bool>();
            List<int> epayresponses = new List<int>();
            List<int> pbsresponses = new List<int>();

            foreach (Subscriptions sub in allSubs)
            {
                authBody.subscriptionid = sub.SubscriptionIDEpay;
                authBody.amount = sub.Price;
                authBody.transactionid = sub.TransactionID;
                authBody.orderid = sub.OrderID.ToString();
                bool authorized = soap.authorize(authBody.merchantnumber, authBody.subscriptionid, authBody.orderid, authBody.amount, authBody.currency, authBody.instantcapture,
                    authBody.group, authBody.description, authBody.email, authBody.sms, authBody.ipaddress, authBody.pwd, authBody.textonstatement,
                    authBody.customerkey, ref authBody.fraud, ref authBody.transactionid, ref authBody.pbsresponse, ref authBody.epayresponse);

                epayresponses.Add(authBody.epayresponse);
                pbsresponses.Add(authBody.pbsresponse);

                authorizations.Add(authorized);
            }


            ViewBag.success = authorizations;
            ViewBag.epayresponse = epayresponses;
            ViewBag.pbsresponse = pbsresponses;

            soap.Close();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult DeleteClubSubscription()
        {
            User user = new User();
            if (User.Identity.IsAuthenticated)
            {
                var GetUserName = User.Identity.Name;
                user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);
                
            }
            soap.Open();
            int merchantNumber = 0;
            int.TryParse(WebConfigurationManager.AppSettings["apiMerchantNumber"], out merchantNumber);
            authBody.merchantnumber = merchantNumber;
            authBody.subscriptionid = user.profile.subscription.SubscriptionIDEpay;
            authBody.orderid = user.profile.subscription.OrderID;
            authBody.amount = user.profile.subscription.Price;
            authBody.currency = 208; //= DKK
            authBody.instantcapture = 0;
            authBody.fraud = 0;
            authBody.pwd = "55141d6f-4079-4d19-b8c6-7a5ec36116a5";

            /*
             * Do not run the one below just yet
             * If the user wants to delete the subscription 
             * then make sure that the rule id = 6 = inactive
             * After that you can delete them from epay etc.
             */

            if(user.profile.subscription.Active == true)
            {
                user.profile.subscription.Active = false;
                //bool deleted = soap.deletesubscription(authBody.merchantnumber, authBody.subscriptionid, authBody.pwd, ref authBody.epayresponse);
            }


            //ViewBag.Deleted = deleted;
            soap.Close();
            
            
            connection.SaveChanges();
            return RedirectToAction("Index", "Home");
        }
    }
}
