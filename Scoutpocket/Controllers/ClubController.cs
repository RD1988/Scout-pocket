﻿using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.Models.Repository;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Dynamic;
using System.Web.Configuration;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;

namespace Scoutpocket.Controllers
{

    public class ClubController : Controller
    {

        private Connection db = new Connection();

        IClubRepository _repository;

        public ClubController() : this(new ClubRepository())
        {

        }

        public ClubController(IClubRepository repository)
        {
            _repository = repository;
        }

        public JsonResult IsUserExists(string UserMail)
        {
            return Json(!db.Users.Any(x => x.UserMail == UserMail), JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddClubList()
        {
            return View();
        }

        //[Route("opret-klub-liste")]
        //[Authorize(Roles = "1")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddClubList(ClubList clubList)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _repository.CreateClubList(clubList);
                }
            }
            catch (Exception ex)
            {


                ModelState.AddModelError("", ex);
                ViewData["CreateError"] = "Unable to create; view innerexception";
            }

            return View("");

        }



        [Route("klub-profil")]
        [Authorize(Roles = "2")]
        public ActionResult ClubProfile()
        {

            if (User.Identity.IsAuthenticated)
            {
                User user = new User();
                Connection connection = new Connection();
                var GetUserName = User.Identity.Name;
                user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);

                int getUserID = user.profile.club.ClubListID;
                System.Web.HttpContext.Current.Session["GetClubID"] = user.profile.club.ClubListID;





                //int getClubID = 

                var profile = connection.Profiles.Where(x => x.ProfileID == getUserID).SingleOrDefault();


                TempData["ID"] = getUserID;

             

            }

            GetClubName();

            PositionDropDownList();
            return View();
        }

        public void PositionDropDownList()
        {

            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "PositionName", "PositionCategory", 0);
        }



        public ActionResult GetClubName()
        {
            int getUserID;
            var GetUserName = User.Identity.Name;
            User user = db.Users.SingleOrDefault(a => a.UserMail == GetUserName);

            getUserID = user.UserID;
            return View(db.Profiles.SingleOrDefault(x => x.ProfileID == getUserID));
        }


        public List<Profile> getProfiles()
        {

            return db.Profiles.ToList();

        }


        public ActionResult GroupByPlayers()
        {

            int getUserID = Convert.ToInt32(TempData["ID"]);

            var profiles = db.Users.ToList();

            var playersGrouped = from p in profiles
                                 where p.RoleID == 3 && p.profile.player.ClubListID == getUserID
                                 orderby p.profile.player.Series.SerieCategories.First().SerieCategoryID, p.profile.player.SeriesID
                                 group p by p.profile.player.Series.SeriesName into g
                                                                
                                 select new Group<string, User> { Key = g.Key.ToString(), Values = g };

            //playersGrouped.ToList()

            return View(playersGrouped.Take(6).ToList());

        }


        [Route("opret-klub")]
        public ActionResult CreateClub()
        {
            ViewBag.ClubListID = new SelectList(db.ClubLists, "ClubListID", "clubName");
            return View();
        }


        [Route("opret-klub")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateClub(ClubProfileViewModel viewModel, ClubList clubList)
        {

            ViewBag.ClubListID = new SelectList(db.ClubLists, "ClubListID", "clubName", clubList.ClubListID);

            Session["viewModel"] = viewModel;

            Session["clublist"] = clubList;

            return RedirectToAction("ChooseClubAbonnement");
        }

        [Authorize(Roles = "2")]
        [Route("aendre-klub-abonnement")]
        public ActionResult ChangeClubSubscription()
        {
            int basicPrice = 0;
            int levelUPPrice = 0;
            int levelUPExclusivePrice = 0;

            //change the prices in web.config
            int.TryParse(WebConfigurationManager.AppSettings["clubPriceBasic"], out basicPrice);
            int.TryParse(WebConfigurationManager.AppSettings["clubPriceLevelUP"], out levelUPPrice);
            int.TryParse(WebConfigurationManager.AppSettings["clubPriceLevelUPExclusive"], out levelUPExclusivePrice);

            ViewBag.Basic = basicPrice;
            ViewBag.LevelUP = levelUPPrice;
            ViewBag.LevelUPExclusive = levelUPExclusivePrice;

            ViewBag.ExistingCategory = 0;

            if (User.Identity.IsAuthenticated)
            {
                int userId = db.Users.SingleOrDefault(x => x.UserMail == User.Identity.Name).profile.ProfileID;
                //try and find the preparedsubscription first. If not found then use the subscription category
                int category = 0;
                try
                {
                    category = db.PreparedSubscriptions.SingleOrDefault(x => x.PrepId == userId).Category;
                }
                catch (Exception)
                {
                    category = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == userId).Category;
                }


                //Since the user changes the subscriptions, then it is better to see the existingcategory by the one he has chosen in preparesubscription. If he changes mind again.
                ViewBag.ExistingCategory = category;
            }
            //because the viewbag did not get the stored value in FindClubPrice method
            Session["Basic"] = basicPrice;
            Session["LevelUP"] = levelUPPrice;
            Session["LevelUPExclusive"] = levelUPExclusivePrice;
            return View();
        }

        [Route("klub-abonnement")]
        public ActionResult ChooseClubAbonnement()
        {
            ViewBag.viewModel = Session["viewModel"] as ClubProfileViewModel;

            //does not really get used but I will leave it here 12-07-2017 - hamun
            ViewBag.clubList = Session["clubList"] as ClubList;

            

            return View();
        }

        /// <summary>
        /// Action Result that handles the choosing of a subscription. This accords for both creating new profiles,
        /// and updating existing profiles. Routes to either a payment window, or clubprofile on updates.
        /// </summary>
        /// <param name="SubscriptionChoice"></param>
        /// <returns></returns>
        [Route("clubsubscriptionchoice/{SubscriptionChoice?}")]
        public ActionResult ClubSubscriptionChoosen(int SubscriptionChoice = 0)
        {
            //Terminate if choice isn't 5 - 7
            if (SubscriptionChoice < 5 || SubscriptionChoice > 7)
            {
                return View(); //Displays an error message
            }

            //Get configuration
            string merchantNumber = WebConfigurationManager.AppSettings["apiMerchantNumber"] as string;
            string currency = WebConfigurationManager.AppSettings["apiCurrency"] as string;
            string acceptURL = "http://scoutpocket.dk/oprettelsen-af-klub";
            int amount;
            amount = FindClubPrice(SubscriptionChoice);
            Session["SubscriptionChoice"] = SubscriptionChoice;


            //Result for a logged in user
            if (User.Identity.IsAuthenticated)
            {
                User user = db.Users.SingleOrDefault(a => a.UserMail == User.Identity.Name);
                Subscriptions userSub = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == user.UserID);

                //If user goes from Basis to Other, goto Payment window
                if (5 < SubscriptionChoice && userSub.Category != 0 && userSub.Category == 5)
                {
                    Session["UpdatingSubscriptionId"] = userSub.SubscriptionID;
                    goto OpenPaymentWindow;
                }
                //If user goes from Other to Basis
                if (SubscriptionChoice == 5)
                {
                    //for free
                    amount = 0;
                    _repository.CreateORUpdatePreparedClubSubscription(user.UserID, amount, false, 1);
                    return RedirectToAction("ClubProfile");
                }
                //If user goes from Other to Other
                if (5 < SubscriptionChoice)
                {
                    _repository.CreateORUpdatePreparedClubSubscription(user.UserID, amount, true, SubscriptionChoice);
                    return RedirectToAction("ClubProfile");
                }

                return RedirectToAction("ClubProfile");
            }
            //Basis shouldn't open a payment window, so it gets intercepted
            if (SubscriptionChoice == 5)
            {
                ClubProfileViewModel viewModel = Session["viewModel"] as ClubProfileViewModel;
                _repository.CreateNonPayingClub(viewModel, SubscriptionChoice, 0); //TODO mailsystem?
                Session["Product"] = "Basis"; //it did not saved it properly before, now it should do it -hamun
                //works here
                SendMail();

                //autologin
                FormsAuthentication.SetAuthCookie(viewModel.UserMail, false);

                Session["Price"] = amount;


                return RedirectToAction("ClubProfile");
            }

            OpenPaymentWindow:

            //Amount sent is 0, actual payments get made via the api at the end of month 
            string hashsource = merchantNumber + "0" + currency + "3" + "1" + acceptURL + WebConfigurationManager.AppSettings["apiHashCode"];

            string redirectParams = string.Format("merchantnumber={0}&amount={1}&currency={2}&windowstate={3}&subscription={4}&accepturl={5}&hash={6}",
                merchantNumber, "0", currency, "3", "1", acceptURL, GetMD5hash(hashsource));

            return Redirect(@"https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx?" + redirectParams);
        }

        private int FindClubPrice(int subscriptionChoice)
        {
            int price = 0;

            int basicPrice = 0;
            int levelUPPrice = 0;
            int levelUPExclusivePrice = 0;

            //change the prices in web.config
            int.TryParse(WebConfigurationManager.AppSettings["clubPriceBasic"], out basicPrice);
            int.TryParse(WebConfigurationManager.AppSettings["clubPriceLevelUP"], out levelUPPrice);
            int.TryParse(WebConfigurationManager.AppSettings["clubPriceLevelUPExclusive"], out levelUPExclusivePrice);

            ViewBag.Basic = basicPrice;
            ViewBag.LevelUP = levelUPPrice;
            ViewBag.LevelUPExclusive = levelUPExclusivePrice;

            //because the viewbag did not get the stored value in FindClubPrice method
            Session["Basic"] = basicPrice;
            Session["LevelUP"] = levelUPPrice;
            Session["LevelUPExclusive"] = levelUPExclusivePrice;

            switch (subscriptionChoice)
            {
                case (5):
                    //free basis
                    price = (int)Session["Basic"];
                    break;
                case (6):
                    price = (int)Session["LevelUP"];
                    break;
                case (7):
                    price = (int)Session["LevelUPExclusive"];
                    break;
            }

            return price;
        }

        public ActionResult SaveProductInSession(string product)
        {
            Session["Product"] = product;
            return Json(new { Status = "Success" });
        }


        [Route("oprettelsen-af-klub")]
        public ActionResult EpayCallbackClub()
        {


            //Get HTTP from epay
            int subscriptionID = 0; // the subscriptionid from epay or subscriptionIDEpay in subscriptions table in db
            int price = 0;
            int transactionID = 0;
            int category = 0;

            string subscriptionIDstring = this.Request.QueryString["subscriptionid"];
            Int32.TryParse(subscriptionIDstring, out subscriptionID);

            string priceString = this.Request.QueryString["amount"];
            Int32.TryParse(priceString, out price);

            string transactionIDString = this.Request.QueryString["txnid"];
            Int32.TryParse(transactionIDString, out transactionID);

            string orderIDString = this.Request.QueryString["orderid"];

            string product = Session["Product"] as string;

            category = (int)Session["SubscriptionChoice"];

            ClubProfileViewModel viewModel = Session["viewModel"] as ClubProfileViewModel;
            // ClubList clublist = Session["clublist"] as ClubList;
            _repository.CreateClub(viewModel, subscriptionID, price, transactionID, orderIDString,category);

            //sendmail
            SendMail();

            //autologin
            FormsAuthentication.SetAuthCookie(viewModel.UserMail, false);
            return RedirectToAction("ClubProfile");
        }


        public JsonResult Search(string Searchtext, int PositionID = 0, int Age = 0, int AgeSpec = 1, int SubProperty = 0, int LH = 0, int LHValue = 0)
        {
            Connection connection = new Connection();
            var GetUserName = User.Identity.Name;
            var user = connection.Users.SingleOrDefault(a => a.UserMail == GetUserName);

            int clubId = user.profile.club.ClubListID;

            if (SubProperty == 0)
            {

                var firstname = new SqlParameter("@firstname", Searchtext);
                var lastname = new SqlParameter("@lastname", Searchtext);

                var result = db.Database
                    .SqlQuery<SearchResult>("[Searchplayer] @firstname , @lastname", firstname, lastname).Where(s => s.ClubListId != clubId)
                    .ToList();

                List<SearchResult> search;
                if (AgeSpec == 1)
                    search = result.Where(p => p.Age >= Age).ToList();
                else
                    search = result.Where(p => p.Age <= Age).ToList();


                //var search =
                //(from p in db.Profiles
                // join pl in db.Players on p.ProfileID equals pl.PlayerID
                // join cl in db.ClubLists on pl.ClubListID equals cl.ClubListID
                // where p.FirstName.Contains(Searchtext) || p.LastName.Contains(Searchtext)
                // select new { p.FirstName, p.LastName, cl.ClubName });


                //ViewBag.SearchResult = search;
                if (search.Count() == 0)
                    search.Add(new SearchResult { FirstName = "", LastName = "", ClubName = "" });
                return Json(new { search }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //var search =
                //(from p in db.Profiles
                // join pl in db.Players on p.ProfileID equals pl.PlayerID
                // join r in db.Ratings on pl.PlayerID equals r.PlayerID
                // join cl in db.ClubLists on pl.ClubListID equals cl.ClubListID
                // join sp in db.SubPropertiess on pl.PositionID equals sp.PositionID
                // where r.SubPropertiesID == SubProperty
                // //&& r.Value == LHValue.ToString()
                // //group p.FirstName, p.LastName, cl.ClubName
                // select new { p.FirstName, p.LastName, cl.ClubName, sp.SubPropertiesID }
                // ).Distinct();

                //search.GroupBy(x => x.SubPropertiesID, x => x.ClubName).ToList();

                if (SubProperty > 0 && LH > 0)
                {


                    var subid = new SqlParameter("@subpropertyid", SubProperty);
                    var value = new SqlParameter("@value", LHValue);
                    var range = new SqlParameter("@range", LH);
                    var result = db.Database
                        .SqlQuery<SearchResult>("[AdvanceSearch] @subpropertyid , @value , @range", subid, value, range).Where(s => s.ClubListId != clubId)
                        .Distinct().ToList();

                    List<SearchResult> search;
                    if (AgeSpec == 1)
                        search = result.Where(p => p.Age >= Age).ToList();
                    else
                        search = result.Where(p => p.Age <= Age).ToList();

                    if (search.Count() == 0)
                        search.Add(new SearchResult { FirstName = "", LastName = "", ClubName = "" });
                    return Json(new { search }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { search = new List<SearchResult> { new SearchResult { FirstName = "", LastName = "", ClubName = "" } } }, JsonRequestBehavior.AllowGet);
            }

            //GetClubName();
            //PositionDropDownList();
            //return View();

            // return Json(new { AdvancedSearch }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendMail()
        {

            //NOTICE, when you change in the database the session might make problems. There make sure that this is correct.
            ClubProfileViewModel viewModel = Session["viewModel"] as ClubProfileViewModel;

            string getProduct = Session["Product"].ToString();

            SmtpClient smtpClient = new SmtpClient();
            //from https://www.lifewire.com/g00/what-are-the-outlook-com-smtp-server-settings-1170671?i10c.referrer=https%3A%2F%2Fwww.google.dk%2F
            //and https://support.office.com/en-us/article/Add-your-Outlook-com-account-to-another-mail-app-73f3b178-0009-41ae-aab1-87b80fa94970
            

            //Send mail, with session

            string getSession = Session["Product"] as string;
            MailMessage mailMessage = new MailMessage();

            MailAddress fromAddress = new MailAddress("scoutpocket@outlook.dk");
            mailMessage.From = fromAddress;

            mailMessage.To.Add(new MailAddress(viewModel.UserMail));
            //Do not know why it does not send to this address?
            mailMessage.To.Add(new MailAddress("scoutpocket@outlook.dk"));


            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = "Velkommen til Scout Pocket";


            if (getSession == "Basis")
            {

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Mails/Scout-pocket-klub-basis.html"))
)
                {
                    mailMessage.Body = reader.ReadToEnd();

                    smtpClient.Send(mailMessage);
                    
                }


            }

            if (getSession == "LevelUP")
            {

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Mails/Scout-pocket-klub-levelUP.html")))
                {
                    
                    mailMessage.Body = reader.ReadToEnd();

                    smtpClient.Send(mailMessage);

                }

            }

            if (getSession == "LevelUpExclusive")
            {

                using (StreamReader reader = new StreamReader(Server.MapPath("~/Mails/Scout-pocket-klub-levelUP-eksklusiv.html")))
                {
                    
                    mailMessage.Body = reader.ReadToEnd();

                    smtpClient.Send(mailMessage);
                }

            }

            
            smtpClient.Dispose();

            return View();
        }


        //GetSubProperties

        public JsonResult GetSubProperties(int id)
        {
            int this_id = id;

            // var subproperties = db.SubPropertiess.Where(s => s.PropertiesID == id).ToList();

            var subproperties = from sp in db.SubPropertiess
                                join pro in db.Propertiess on sp.PropertiesID equals pro.PropertiesID
                                where sp.PositionID == id
                                select new { sp.SubPropertiesID, sp.SubPropertiesName, pro.PropertiesName };



            var jsondata = subproperties.ToList();

            Console.WriteLine(jsondata);
            // string name = this_name.productName;
            // do here some operation  
            return Json(new { jsondata }, JsonRequestBehavior.AllowGet);
        }


        public string GetMD5hash(string source)
        {
            StringBuilder sBuild = new StringBuilder();
            using (var md5hash = MD5.Create())
            {
                byte[] data = md5hash.ComputeHash(Encoding.UTF8.GetBytes(source));


                for (int i = 0; i < data.Length; i++)
                {
                    sBuild.Append(data[i].ToString("x2"));
                }
            }

            return sBuild.ToString();
        }


    }
}
 