﻿using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.Models.Repository;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace scoutpocket.Controllers
{
    public class ScoutController : Controller
    {
        private Connection db = new Connection();

        IScoutRepository _repository;

        public ScoutController() : this(new ScoutRepository())
        {

        }

        public ScoutController(IScoutRepository repository)
        {
            _repository = repository;
        }
        public JsonResult IsUserExists(string UserMail)
        {
            return Json(!db.Users.Any(x => x.UserMail == UserMail), JsonRequestBehavior.AllowGet);
        }


        [Route("scout-profil/{search?}")]
     
        [Authorize(Roles = "4")]
        public ActionResult ScoutProfile(string search)
        {


            ViewData["ControllerName"] = this.ToString();
            return View("ScoutProfile", _repository.SearchPlayers(search));
        }

        [Route("vurder-spiller/{id?}")]
        public ActionResult AddPlayerProperties(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Profile pro = _repository.GetProfileById(id);

            if (pro == null)
            {
                return HttpNotFound();
            }

            ViewBag.Keeper = "SQL string";

            return View("AddPlayerProperties", pro);
        }

        public void ValueDropDownList()
        {
            List<SelectListItem> valueitems = new List<SelectListItem>();

            valueitems.Add(new SelectListItem { Text = "1", Value = "1" });

            valueitems.Add(new SelectListItem { Text = "2", Value = "2" });

            valueitems.Add(new SelectListItem { Text = "3", Value = "3" });

            valueitems.Add(new SelectListItem { Text = "4", Value = "4" });

            valueitems.Add(new SelectListItem { Text = "5", Value = "5" });

            valueitems.Add(new SelectListItem { Text = "6", Value = "6" });

            valueitems.Add(new SelectListItem { Text = "7", Value = "7" });

            valueitems.Add(new SelectListItem { Text = "8", Value = "8" });

            valueitems.Add(new SelectListItem { Text = "9", Value = "9" });

            valueitems.Add(new SelectListItem { Text = "10", Value = "10" });


            valueitems.Add(new SelectListItem { Text = "11", Value = "11" });

            valueitems.Add(new SelectListItem { Text = "12", Value = "12" });

            valueitems.Add(new SelectListItem { Text = "13", Value = "13" });

            valueitems.Add(new SelectListItem { Text = "14", Value = "14" });

            valueitems.Add(new SelectListItem { Text = "15", Value = "15" });

            valueitems.Add(new SelectListItem { Text = "16", Value = "16" });

            valueitems.Add(new SelectListItem { Text = "17", Value = "17" });

            valueitems.Add(new SelectListItem { Text = "18", Value = "18" });

            valueitems.Add(new SelectListItem { Text = "19", Value = "19" });


            valueitems.Add(new SelectListItem { Text = "20", Value = "20" });

            ViewBag.Value = valueitems.ToList();






        }

        [Route("vurder-egenskaber/{id?}")]
        public ActionResult RateProperties(int? id)
        {

            var getID = id;

            ViewBag.PlayerID = getID;

            ScoutController sc = new ScoutController();
            PropertiesViewModel pv = new PropertiesViewModel();

            pv.profile = sc.getProfiles();


            return View();
        }

        public List<Profile> getProfiles()
        {

            return db.Profiles.ToList();

        }


        public List<Properties> getProperties()
        {
            return db.Propertiess.ToList();
        }

        public List<SubProperties> getSubproperties()
        {
            return db.SubPropertiess.ToList();
        }





        [Route("angiv-egenskaber")]
        public ActionResult GetProperties()
        {


            ValueDropDownList();




            var MultipleModel = new Tuple<List<Properties>, List<SubProperties>, List<Profile>>
            (db.Propertiess.ToList(), db.SubPropertiess.ToList(), db.Profiles.ToList())
            {


            };


            return View(MultipleModel);
        }

        [HttpPost]
        public void CommentProperties(int propertyId, int playerId, string comment)
        {
            throw new NotImplementedException();
            
        }
        [HttpPost]
        public ActionResult Proptie(FormCollection form1)
        {

            string Valueid = form1.Get("Value");
            string playerid = form1.Get("PlayerID");
            //string playerbirth = form1.Get("PlayerBirth");
            string ScoutID = form1.Get("ScoutID");
            string SubPropertiesID = form1.Get("SubPropertiesID");

            string[] value = Valueid.Split(',');
            string [] players = playerid.Split(',');
          //  string[] playerbirth = playerbirth.Split(',');
            string[] Scouts = ScoutID.Split(',');
            string[] SubProperties = SubPropertiesID.Split(',');
            var scoutComments = form1.GetValues("PropertyComment");
            string _propertyIds = form1.Get("PropertyId");
            string[] propertyIds = _propertyIds.Split(',');



            using (var db = new Connection())
            {
                for (int i = 0; i < value.Length; i++)
                {

                    var t = new Rating
                    {
                        Value = value[i].ToString(), // Adds the value from the dropdownlist
                        PlayerID = int.Parse(players[i].ToString()), // Adds the players id
                        ScoutID = int.Parse(Scouts[i].ToString()), // Adds the Scouts' id
                        SubPropertiesID = int.Parse(SubProperties[i].ToString()), // Adds the Scouts' id
                        
                        //parse player dith to rating table to.
                        Date = DateTime.Now // Adds the current date
                    };
                   
                    db.Ratings.Add(t);


                }
                var player = db.Players.Find(int.Parse(players.First()));
                var scout = db.Users.Find(int.Parse(Scouts.First()));
               
                for(int i = 0; i < propertyIds.Length; i++)
                {
                    var property = db.Propertiess.Find(int.Parse(propertyIds[i].ToString()));
                    var c = new PlayerPropertyScoutComment
                    {
                        Comment = scoutComments[i].ToString(),
                        Player = player,
                        Scout = scout,
                        Property = property,
                        Created = DateTime.Now
                        
                    };
                    db.PlayerPropertyScoutComment.Add(c);
                }
                
                db.SaveChanges();

            }


            ValueDropDownList();




            var MultipleModel = new Tuple<List<Properties>, List<SubProperties>, List<Profile>>
            (db.Propertiess.ToList(), db.SubPropertiess.ToList(), db.Profiles.ToList())
            {


            };
            //return View("GetProperties");
          return  RedirectToAction("ScoutProfile"); 
           
        }



        // GET: Scout
        public ActionResult Index()
        {
            ViewData["ControllerName"] = this.ToString();
            return View("Index", _repository.GetAllScouts());
        }




        public ActionResult CreateScout()
        {
            return View();
        }

        [ValidateInput(false)]

        [HttpPost]
        public ActionResult CreateScout(ScoutProfileViewModel viewModel)
        {
            try
            {
                _repository.CreateScout(viewModel);

                ViewBag.Succes = "Scout profilen er nu oprettet, der er sendt en mail til brugeren";
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex);
                ViewData["CreateError"] = "Unable to create; view innerexception";
            }


            return View();
        }


    }
}