﻿
    function HideSubropertyDIv() {


        $(".cbCheck").each(function (i) {
            this.checked = false;
        });
        document.getElementById("subProperties").style.display = "none";

    }


    function checkBoxDemo() {



        var hiddenDiv = document.getElementById("subProperties");
        hiddenDiv.style.display = (this.value == "") ? "none" : "block";
    }

    function showAge() {

        var hiddenDiv = document.getElementById("age");
        hiddenDiv.style.display = (this.value == "") ? "none" : "block";

        return false;
    }

    function showSearchResults() {

        var hiddenDiv = document.getElementById("searchResults");
        hiddenDiv.style.display = (this.value == "") ? "none" : "block";

    }



    // Show Subproperties on selection of properties

    function CallChangefunc() {

        var selectedValue = $("#ddlPosition").val();
        $.ajax(
            {
                type: 'POST',
                dataType: 'json',

                url: '/Club/GetSubProperties',
                data: { id: selectedValue },
                success: function (data) {

                    // debugger;

                    /////////////
                    var strResult = "<table class='table table-condensed' style=color:white;><tr>";
                    $.each(data, function (index, jsondata) {

                        var names = [];
                        var uniqueNames = [];

                        for (var i = 0; i < data.jsondata.length; i++) {
                            names.push(jsondata[i].PropertiesName);

                            $.each(names, function (i, el) {
                                if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                            });
                        }


                        for (var i = 0; i < uniqueNames.length; i++) {
                            strResult += "<th id=" + uniqueNames[i] + ">" + uniqueNames[i] + "</th>";
                        }

                        strResult += "</tr>";


                        //Createing Rows
                        var dataList = {};
                        var maxLength = 0;
                        for (var j = 0; j < uniqueNames.length; j++) {
                            var pName = uniqueNames[j];
                            var obj = [];
                            for (var i = 0; i < data.jsondata.length; i++) {
                                var propertyname = jsondata[i].PropertiesName;
                                if (pName == propertyname)
                                    obj.push(jsondata[i]);
                            }
                            dataList[pName] = obj;
                            if (obj.length >= maxLength) {
                                maxLength = obj.length;
                            }
                        }
                        //console.log(dataList);
                        for (var k = 0; k < maxLength; k++) {
                            strResult += "<tr>";
                            for (var key in dataList) {
                                if (typeof dataList[key][k] !== "undefined") {
                                    strResult += "<td><input class=cbCheck  type=radio name=sp id = " + dataList[key][k].SubPropertiesID + " value =" + dataList[key][k].SubPropertiesName + "  onclick = checkboxalert() /> " + dataList[key][k].SubPropertiesName + "</td>";
                                } else {
                                    strResult += "<td></td>";
                                }
                            }
                            strResult += "</tr>";
                        }



                        //strResult += "<tr>";
                        //for (var i = 0; i < data.jsondata.length; i++) {

                        //    var propertyname = jsondata[i].PropertiesName

                        //    for (var j = 0; j < uniqueNames.length; j++) {
                        //        if (uniqueNames[j] == propertyname) {
                        //            strResult += "<td><input class=cbCheck  type=radio name=sp id = " + jsondata[i].SubPropertiesID + " value =" + jsondata[i].SubPropertiesName + "  onclick = checkboxalert() /> " + jsondata[i].SubPropertiesName + "</td>"

                        //        }

                        //        //else {
                        //        //    strResult +=" <td></td>"
                        //        //}

                        //    }

                        //}
                    });

                    strResult += "</table>";
                    $("#ddlShow").html(strResult);
                    var hiddenDiv = document.getElementById("ddlShow");
                    var hiddenContainer = document.getElementById("ddlContainerShow");
                    var hiddenSpan = document.getElementById("closeddlButton");
                    hiddenSpan.style.display = (this.value == "") ? "none" : "block";
                    hiddenContainer.style.display = (this.value == "") ? "none" : "block";
                    hiddenDiv.style.display = (this.value == "") ? "none" : "block";


                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                }
            });
    }

    function checkboxalert() {


        $('.cbCheck').each(function () {
            if ($(this).is(':checked')) {
                //  alert($(this).attr('id'));// checked item id
                document.getElementById('subProperties').style.display = "block";
                document.getElementById('subpropertiesID').innerHTML = $(this).attr('id');
                document.getElementById('subpropertiesName').innerHTML = $(this).attr('value');

            }
        });
    }

    function showSearch() {



        document.getElementById("showSearchWell").style.display = "block";
    }


    function runSearch()
    {
        showSearch();
        debugger;

        var searchtext = $("#searchtext").val();

        // var e = document.getElementById("ddlPosition");
        var positionid = $("#ddlPosition").val();
        var subproperty = $("#subpropertiesID").text();
        var ageText = $('#ageSelect').val();
        var ageSpec = $('#ageSpec').val();
        var lh = $("#LH").val();
        var lhvalue = $("#LHValue :selected").text();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Club/Search',
            data: { Searchtext: searchtext, PositionID: positionid, Age: ageText, AgeSpec: ageSpec, Subproperty: subproperty, LH: lh, LHValue: lhvalue },
            success: function (data) {
                //  debugger;

                if (data != null) {
                    var strResult = "<table class=table style=color:white;><th>Fornavn</th><th>Efternavn</th><th>Klub</th>";
                    for (var i = 0; i < data.search.length; i++)
                    {
                        //Check the id of the user, and positionID, and parse it too two sessions.

                        var url_firstname = "<a href='../spiller-profil/" + data.search[i].ProfileID + "'><span>  " + data.search[i].FirstName + " </span></a>";
                        var url_lastname = "<a href='../spiller-profil/" + data.search[i].ProfileID + "'><span>  " + data.search[i].LastName + " </span></a>";
                        strResult += "<tr>  <td> " + url_firstname + "</td><td>" + url_lastname + "</td><td>" + data.search[i].ClubName + "</td></tr>";

                    
                    }

                    strResult += "</table>";
                    $("#divResult").html(strResult);
                    // alert(strResult);
                }
                else {
                    $("#divResult").html("No Results To Display");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }