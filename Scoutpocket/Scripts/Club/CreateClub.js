﻿

jQuery(document).ready(function () {

    jQuery("#ClubListID").change(function () {
        var getClubID = $jQuery('#ClubListID').find('option:selected').val();
        //document.getElementById("getHiddenSerieID").value = getClubID;
        //alert(getClubID);
    });
});


var CreateClubURL = jQuery('#CreateClubURL').attr('data-attribute');
var form = document.getElementById('formID'); // form has to have ID: <form id="formID">
form.addEventListener('submit', function (event) { // listen for form submitting
    if (!event.target.checkValidity()) {
        event.preventDefault(); // dismiss the default functionality




        var selected_clubListID = $('#ClubListID').val()
        if (selected_clubListID == 0) {
            document.getElementById('errorClub').style.display = 'block';
        }

        if ($('#FirstName').val() == '') {
            document.getElementById('errorName').style.display = 'block';
        }

        if ($('#Phone').val() == '') {
            document.getElementById('errorPhone').style.display = 'block';
        }

        if ($('#UserMail').val() == '') {
            document.getElementById('errorMail').style.display = 'block';
        }

        if ($('#UserPassword').val() == '') {
            document.getElementById('errorPass').style.display = 'block';
        }

        if ($('#UserPasswordRepeat').val() == '') {
            document.getElementById('errorPassRepeat').style.display = 'block';
        }


        var UserPassword = document.getElementById("UserPassword");
        var UserPasswordRepeat = document.getElementById("UserPasswordRepeat");

        if (UserPassword.value != UserPasswordRepeat.value || UserPasswordRepeat.value != UserPassword.value) {
            document.getElementById('errorPassCompare').style.display = 'block';
        }



        if (remember.checked == false) {
            //alert("checked");
            document.getElementById('errorMessageCH').style.display = 'block';
        }




        // error message
        return true;


    }
    else {
        $.post(CreateClubURL);
    }

}, false);