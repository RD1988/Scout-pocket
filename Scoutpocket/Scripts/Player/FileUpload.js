﻿
//************************************** JavaScript for ajax file upload **************************************
var jqXHRData;

$(document).ready(function () {

    'use strict';


    $('#fu-my-simple-upload').fileupload({
        url: '/File/UploadFile',
        dataType: 'json',
        add: function (e, data) {
            jqXHRData = data;
        },
        done: function (event, data) {
            if (data.result.isUploaded) {

                $("#hf-uploaded-image-path").val(data.result.filePath);

                destroyCrop();

                $("#uploaded-image").attr("src", data.result.filePath + "?t=" + new Date().getTime());

                initCrop();

                $("#crop-image-area").fadeIn("slow");
            } else {

            }
        },
        fail: function (event, data) {
            if (data.files[0].error) {
                alert(data.files[0].error);
            }
        }
    });
});

$("#hl-start-upload").on('click', function () {
    if (jqXHRData) {
        jqXHRData.submit();
    }
    return false;
});