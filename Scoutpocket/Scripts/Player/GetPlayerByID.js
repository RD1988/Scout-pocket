﻿
//************************************** JavaScript for ajax file upload **************************************
var jqXHRData;

$(document).ready(function () {

    'use strict';


    $('#fu-my-simple-upload').fileupload({
        url: '/File/UploadFile',
        dataType: 'json',
        add: function (e, data) {
            jqXHRData = data;
        },
        done: function (event, data) {
            if (data.result.isUploaded) {

                $("#hf-uploaded-image-path").val(data.result.filePath);

                destroyCrop();

                $("#uploaded-image").attr("src", data.result.filePath + "?t=" + new Date().getTime());

                initCrop();

                $("#crop-image-area").fadeIn("slow");
            } else {

            }
        },
        fail: function (event, data) {
            if (data.files[0].error) {
                alert(data.files[0].error);
            }
        }
    });
});

$("#hl-start-upload").on('click', function () {
    if (jqXHRData) {
        jqXHRData.submit();
    }
    return false;
});
//************************************** JavaScript for ajax file upload END **************************************



//************************************** JavaScript for cropping of image *****************************************
var imageCropWidth = 0;
var imageCropHeight = 0;
var cropPointX = 0;
var cropPointY = 0;

$("#hl-crop-image").on("click", function (e) {
    e.preventDefault();
    cropImage();
});

function initCrop() {
    $('#uploaded-image').Jcrop({
        onChange: setCoordsAndImgSize,
        aspectRatio: 1
    });
}

function destroyCrop() {
    var jcropApi = $('#uploaded-image').data('Jcrop');

    if (jcropApi !== undefined) {
        jcropApi.destroy();
        $('#uploaded-image').attr('style', "").attr("src", "");
    }
}

function setCoordsAndImgSize(e) {

    imageCropWidth = e.w;
    imageCropHeight = e.h;

    cropPointX = e.x;
    cropPointY = e.y;
}

function cropImage() {

    if (imageCropWidth == 0 && imageCropHeight == 0) {
        alert("Please select crop area.");
        return;
    }

    $.ajax({
        url: '/Image/CropImage',
        type: 'POST',
        data: {
            imagePath: $("#hf-uploaded-image-path").val(),
            cropPointX: cropPointX,
            cropPointY: cropPointY,
            imageCropWidth: imageCropWidth,
            imageCropHeight: imageCropHeight
        },
        success: function (data) {

            $("#hf-cropped-image-path").val(data.photoPath);

            $("#my-cropped-image")
                .attr("src", data.photoPath + "?t=" + new Date().getTime())
                .show();

            $("#btn-my-submit").fadeIn("slow");
        },
        error: function (data) { }
    });
}

    //************************************** JavaScript for cropping of image END **************************************