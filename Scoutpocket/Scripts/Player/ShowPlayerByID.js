﻿function setData(own, averageLabels) {
    var origin = own;
    var values = [];
    var average = [];
    for (var i = 1; i < own.length; i += 1) {
        var tmp = own.splice(i, 1);
        var tmp1 = averageLabels.splice(i, 1);
        average.push(tmp1);
        values.push(tmp);
    }
    values = [].concat.apply([], values);

    return {
        labels: own,
        datasets: [{
            data: values,
            label: 'Egen vurdering',
            borderColor: 'rgba(174, 177, 54, 1)',
            pointBorderColor: 'rgba(174, 177, 54, 1)',
            backgroundColor: 'rgba(174, 177, 54, 0.5)',
            pointBackgroundColor: 'rgba(174, 177, 54, 1)',
            pointRadius: 3,
            pointHitRadius: 3,
            pointBorderWidth: 1,
            pointHoverBorderWidth: 1,
        },
        {
            label: 'Gennemsnit',
            data: average,
            borderColor: 'rgba(53, 127, 177, 1)',
            pointBorderColor: 'rgba(53, 127, 177, 1)',
            backgroundColor: 'rgba(53, 127, 177, 0.5)',
            pointBackgroundColor: 'rgba(53, 127, 177, 1)',
            pointRadius: 3,
            pointHitRadius: 3,
            pointBorderWidth: 1,
            pointHoverBorderWidth: 1,
        }
        ]
    }
}


//////////////////////////////////////////////////////////////
//////////////////////// Set-Up //////////////////////////////
//////////////////////////////////////////////////////////////
var margin = { top: 50, right: 75, bottom: 50, left: 75 },
    width = Math.min(200, window.innerWidth - 10) - margin.left - margin.right,
    height = Math.min(width, window.innerHeight - margin.top - margin.bottom - 20);

//////////////////////////////////////////////////////////////
////////////////////////// Data //////////////////////////////
//////////////////////////////////////////////////////////////
/*var data = [
                [//iPhone
                    { axis: "Battery Life", value: 15 },
                    { axis: "Brand", value: 12 },
                    { axis: "Contract Cost", value: 20 },
                    { axis: "Design And Quality", value: 16 },
                    { axis: "Have Internet Connectivity", value: 14 },
                    { axis: "Large Screen", value: 10 },
                    { axis: "Price Of Device", value: 9 },
                    { axis: "To Be A Smartphone", value: 8 }
                ]
];*/



var radarObject = $('#radarObject').data('attribute');
var jsonRadarDataTeknisk = $('#radarObjectTeknisk').data('attribute');
var jsonRadarDataMental = $('#radarObjectMental').data('attribute');
var jsonRadarDataFysisk = $('#radarObjectFysisk').data('attribute');

//For average radarchart
var jsonRadarAverageTeknisk = $('#radarObjectAverageTeknisk').data('attribute');
var jsonRadarAverageMental = $('#radarObjectAverageMental').data('attribute');
var jsonRadarAverageFysisk = $('#radarObjectAverageFysisk').data('attribute');

if (radarObject !== null)
{
    $('#popoverRadarData-0').webuiPopover({ url: '#Radar-0', placement: 'bottom', height: '220' });
    $('#popoverRadarData-1').webuiPopover({ url: '#Radar-1', placement: 'bottom', height: '220' });
    $('#popoverRadarData-2').webuiPopover({ url: '#Radar-2', placement: 'bottom', height: '220' });

    Chart.defaults.global.defaultFontSize = 10;
    Chart.defaults.global.defaultFontColor = '#FFF';

    var radar0Data = setData(jsonRadarDataTeknisk, jsonRadarAverageTeknisk)
    var radar1Data = setData(jsonRadarDataMental, jsonRadarAverageMental)
    var radar2Data = setData(jsonRadarDataFysisk, jsonRadarAverageFysisk)


    var radarOptions = {
        responsive: false,
        scale: {
            ticks: {
                beginAtZero: true,
                max: 20,
                stepSize: 5,
                display: false,
            },
            gridLines: {
                color: '#FFFFF'
            },
            angleLines: {
                display: true,
                color: '#FFFFF',
                lineWidth: 1
            },

        },
        tooltips: {
            filter: function (tooltipItem) {
                return tooltipItem.datasetIndex === 0;
            }
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgba(255, 255, 255,1)',
                fontSize: 8,
                boxWidth: 1,

            },
            position: 'right',
        }
    };
    var radar0 = document.getElementById("Radar-0");
    var radar1 = document.getElementById("Radar-1");
    var radar2 = document.getElementById("Radar-2");


}



//////////////////////////////////////////////////////////////
//////////////////// Draw the Chart //////////////////////////
//////////////////////////////////////////////////////////////
if (radarObject !== null)
{
    var radar0 = new Chart(radar0, {
        type: 'radar',
        data: radar0Data,
        options: radarOptions
    });
    var radar1 = new Chart(radar1, {
        type: 'radar',
        data: radar1Data,
        options: radarOptions
    });
    var radar2 = new Chart(radar2, {
        type: 'radar',
        data: radar2Data,
        options: radarOptions
    });

    jQuery("#Radar-0").hide();
    $('#popoverRadarData-0').click(function () {
        jQuery("#Radar-0").show();
    });
    jQuery("#Radar-1").hide();
    $('#popoverRadarData-1').click(function () {
        jQuery("#Radar-1").show();
    });
    jQuery("#Radar-2").hide();
    $('#popoverRadarData-2').click(function () {
        jQuery("#Radar-2").show();
    });

}
