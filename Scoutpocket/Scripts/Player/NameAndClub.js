﻿
$(document).ready(function () {
    var originalClubName = jQuery('#TheClubName').attr('data-attribute');
    var originalSeriesName = jQuery('#TheSeriesName').attr('data-attribute');
    var seriesCategory = jQuery('#TheSeriesCategory').attr('data-attribute');
    var regions = jQuery('#regions').attr('data-attribute');
    var getSeriesActionUrl = jQuery('#getSeries').attr('data-attribute');
    var getSeriesIdActionUrl = jQuery('#getSeriesID').attr('data-attribute');
    var TheSeriesID = jQuery('#TheSeriesID').attr('data-attribute');
    var TheClubListID = jQuery('#TheClubListID').attr('data-attribute');
    var TheRegionID = jQuery('#TheRegionID').attr('data-attribute');
    var originalRegionName = jQuery("#OrginialRegionName").attr('data-attribute');
    var UpdatePlayerActionURL = jQuery("#UpdatePlayerActionURL").attr('data-attribute');

    var originalRegionID = jQuery('#OriginalRegionID').attr('data-attribute');
    var originalClubID = jQuery('#OriginalClubID').attr('data-attribute');
    var originalSeriesID = jQuery('#OriginalSeriesID').attr('data-attribute');
    var originalSeriesCategoryID = jQuery('#OriginalSeriesCategoryID').attr('data-attribute');

    originalClubName = originalClubName.replace(/\"/g, "");
    originalSeriesName = originalSeriesName.replace(/\"/g, "");
    originalRegionName = originalRegionName.replace(/\"/g, "");


    seriesCategory = jQuery.parseJSON(seriesCategory);
    regions = jQuery.parseJSON(regions);


    //set the selected value for the update player
    var originalRegionID = parseInt(originalRegionID);
    var originalSeriesCategoryID = parseInt(originalSeriesCategoryID);
    var originalSeriesID = parseInt(originalSeriesID);
    var originalClubID = parseInt(originalClubID);

    var getRegionsActionURL = jQuery('#GetRegions').attr("data-attribute");


    jQuery("#RegionID").val(originalRegionID);
    jQuery("#SeriesCategory").val(originalSeriesCategoryID);
    jQuery("#seriess").val(originalSeriesID);
    jQuery("#ClubListID").val(originalClubID);

    $("#SeriesCategory").change(function () {
        if (RegionAndSeriesCategoryHasValues()) {
            UpdateSeriesAjax();
            ResetRegionAndSeries();
        }
    });
    $("#RegionID").change(function () {
        if (RegionAndSeriesCategoryHasValues())
        {
            UpdateSeriesAjax();

        }


    });
    $("#seriess").change(function () {
        var getHiddenSerieID = document.getElementById("seriess").value;
        $.ajax({
            type: 'POST',
            url: getSeriesIdActionUrl,
            dataType: 'json',
            data: { id: $("#seriess").val() },
            error: function (ex) {
                alert('Failed to retrieve states.' + ex);
            }
        });
    });
    $('#editClubBtn').on('click', function () {
        $('#changeClubModal').modal('show');
    });




    $('#saveNewClub').on('click', function () {
        clubID = $('#ClubListID').val();
        seriesID = $('#seriess').val();
        regionID = $('#RegionID').val();

        if (!clubID) {
            clubID = 0;
        }
        if (!seriesID) {
            seriesID = 0;
        }
        if (!regionID) {
            regionID = 0;
        }
        var dto = {
            clubID: clubID,
            seriesID: seriesID,
            regionID: regionID 
        };
        if (clubID == 0 || seriesID == 0 || regionID == 0) {
            alert("Du har IKKE udfyldt alle felter");
        }
        else {
            $.ajax({
                contentType: 'application/json',
                data: JSON.stringify(dto),
                success: function () {

                    var clubName = $('#ClubListID  option:selected').text();
                    var series = $('#seriess  option:selected').text();
                    var regionName = $("#RegionID option:selected").text();

                    if (clubName === "Vælg en klub") {
                        clubName = originalClubName;
                    }
                    if (series === "Vælg serie/årgang") {
                        series = originalSeriesName;
                    }
                    if (regionName === "Vælg landsdel") {
                        regionName = originalRegionName;
                    }
                    $('#clubName').text(clubName + ' (' + series + ') ');

                    $('#changeClubModal').modal('hide');
                },
                error: function () {
                    location.reload();
                },
                type: 'POST',
                url: UpdatePlayerActionURL
            });
        }
    });

    function ResetRegionAndSeries() {
        $("#RegionID").val($("#RegionID option:first").val());
        $("#SeriesCategory").val($("#SeriesCategory option:first").val());
    }

    function RegionAndSeriesCategoryHasValues()
    {

        var regionID = $("#RegionID").val();
        var seriesCategoryID = $("#SeriesCategory").val();
        if (regionID != "" && seriesCategoryID != "") {
            return true;
        }
        return false;
    }
   
    function UpdateSeriesAjax() {
        $("#seriess").empty();
        $.ajax({
            type: 'POST',
            url: getSeriesActionUrl,
            dataType: 'json',
            data: { seriesCategoryID: $("#SeriesCategory").val(), regionID: $("#RegionID").val() },
            success: function (series) {
                $.each(series, function (i, series) {
                    $("#seriess").append('<option value="' + series.Value + '">' + series.Text + '</option>');

                });
            },
            error: function (ex) {
                alert('Failed.' + ex);
            }

        });

        return false;
    }

});
