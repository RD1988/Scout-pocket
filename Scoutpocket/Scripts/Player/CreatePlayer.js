﻿var getSeriesCategoriesActionURL = jQuery('#getSeriesCategories').attr('data-attribute');
$(document).ready(function () {

    //date time picker in the create player page.
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startView: 'decade',
        endDate: "2017"
    });
    //gender picker
    $("#gender").change(function () {
        $("#SeriesCategory").empty();
        $.ajax({
            type: 'POST',
            url: getSeriesCategoriesActionURL,

            dataType: 'json',

            data: { id: $("#gender").val() },


            success: function (SeriesCategoies) {


                $.each(SeriesCategoies, function (i, SeriesCategory) {
                    $("#SeriesCategory").append('<option value="' + SeriesCategory.Value + '">' +
                        SeriesCategory.Text + '</option>');

                });
            },
            error: function (ex) {
                alert('Failed to retrieve states.' + ex);
            }
        });
        return false;
    });

   
    //series category picker, show a list of regions
    var getRegionsActionURL = jQuery('#GetRegions').attr("data-attribute");
    $("#SeriesCategory").change(function () {
        $("#RegionID").empty();
        $.ajax({
            type: 'POST',
            url: getRegionsActionURL,
            dataType: 'json',
            data: {},
            success: function (regions) {
                $.each(regions, function (i, regions) {
                    $("#RegionID").append('<option value="' + regions.Value + '">' + regions.Text + '</option>');
                });
            },
            error: function (ex) {
                alert('Failed.' + ex);
            }

        });

        return false;

    });

    var getSeriesActionURL = jQuery('#getSeries').attr("data-attribute");
    //Region picker get a list of series
    $('#RegionID').change(function () {
        $("#Seriess").empty();
        $.ajax({
            type: 'POST',
            url: getSeriesActionURL,
            dataType: 'json',
            data: { seriesCategoryID: $("#SeriesCategory").val(), regionID: $("#RegionID").val() },
            success: function (series) {
                console.log(series);

                $.each(series, function (i, series) {
                    $("#Seriess").append('<option value="' + series.Value + '">' + series.Text + '</option>');
                });
            },
            error: function (ex) {
                alert('Failed.' + ex);
            }
        });
    });

    // series picker 
    var getSeriesIDActionURL = jQuery('#getSeriesID').attr("data-attribute");
    $("#Seriess").change(function () {

        $.ajax({
            type: 'POST',
            url: getSeriesIDActionURL,

            dataType: 'json',

            data: { id: $("#Seriess").val() },
            success: function (SeriesCategoies) {
            },
            error: function (ex) {
                alert('Failed to retrieve states.' + ex);
            }
        });


    });



    //for creating form

    var form = document.getElementById('formID'); // form has to have ID: <form id="formID">
    var createPlayerActionURL = jQuery('#cratePlayer').attr("data-attribute");
    form.addEventListener('submit', function (event) { // listen for form submitting
        if (!event.target.checkValidity()) {
            event.preventDefault(); // dismiss the default functionality






            if ($('#FirstName').val() == '') {
                document.getElementById('errorFirstName').style.display = 'block';
            }

            if ($('#LastName').val() == '') {

                document.getElementById('errorLastname').style.display = 'block';
            }

            if ($('#BirthDate').val() == '') {
                document.getElementById('errorBirth').style.display = 'block';
            }

            var selected_gender = $('#gender').val()
            if (selected_gender == 0) {
                document.getElementById('errorGender').style.display = 'block';
            }

            var selected_seriesCategory = $('#SeriesCategory').val()
            if (selected_seriesCategory == 0) {
                document.getElementById('errorSeriesCategory').style.display = 'block';
            }

            var selected_seriesCategory = $('#RegionID').val()
            if (selected_seriesCategory == 0) {
                document.getElementById('errorRegionID').style.display = 'block';
            }

            var selected_seriess = $('#seriess').val()
            if (selected_seriess == 0) {
                document.getElementById('errorSerie').style.display = 'block';
            }

            var selected_clubListID = $('#ClubListID').val()
            if (selected_clubListID == 0) {
                document.getElementById('errorClubList').style.display = 'block';
            }

            var selected_clubListID = $('#ClubListID').val()
            if (selected_clubListID == 0) {
                document.getElementById('errorClubList').style.display = 'block';
            }

            var selected_positionID = $('#PositionID').val()
            if (selected_positionID == 0) {
                document.getElementById('errorPositon').style.display = 'block';
            }

            if ($('#UserMail').val() == '') {
                document.getElementById('errorMail').style.display = 'block';
            }



            // dont work
            if ($("#UserPassword").val() !== $("#UserPasswordRepeat").val()) {
                alert('Hallo');
                return false;
            }


            if (remember.checked == false) {
                //alert("checked");
                document.getElementById('errorCH').style.display = 'block';
            }


            // error message
            return false;
        }
        else {
            $.post(createPlayerActionURL);
        }

    }, false);

   
});