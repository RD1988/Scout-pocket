﻿using System;
using Scoutpocket;
using scoutpocket;
using Hangfire;
using Hangfire.Logging;
using System.Linq;
using System.Web.Configuration;
using Scoutpocket.EpaySubscription;
using Scoutpocket.Models.Database;
using System.Collections.Generic;

namespace Scoutpocket.RecurringJobs
{
    /// <summary>
    /// Contains methods that are used by the hangfire framework. Invoked in Global startup.
    /// </summary>
    public class HangfireRoutines
    {
        public static DateTime TimeTest()
        {
            DateTime now = DateTime.Now;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/Logs/log.txt"), true))
            {
                file.WriteLine("Timestamp: " + now.ToString());
            }
            return now;
        }


        // Rolls PreparedSubscriptions over to Subscriptions and deletes all PreparedSubs after
        public static int UpdateSubscriptions()
        {
            using (var db = new Connection())
            {
                if (!db.PreparedSubscriptions.Any())
                {
                    return 0;
                }


                var results = from x in db.Subscriptions
                              join y in db.PreparedSubscriptions on x.SubscriptionID equals y.PrepId
                              select new { x, y };

                foreach (var item in results)
                {
                    item.x.Active = item.y.Active;
                    item.x.Category = item.y.Category;
                    item.x.OrderID = item.y.OrderID;
                    item.x.Price = item.y.Price;
                    item.x.SubscriptionIDEpay = item.y.SubscriptionIDEpay;
                    item.x.TransactionID = item.y.TransactionID;
                }
                db.SaveChanges();

                db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.PreparedSubscriptions");
            }
            return 0;
        }

        //Make payments - Epay.Authorize()
        public static int AuthorizeAllSubscriptions()
        {
            int mNumber = 0;
            int.TryParse(WebConfigurationManager.AppSettings["apiMerchantNumber"], out mNumber);
            int apiCurr = 208;
            int.TryParse(WebConfigurationManager.AppSettings["apiCurrency"], out apiCurr);
            string apiEmail = WebConfigurationManager.AppSettings["apiCurrency"];

            using (var db = new Connection())
            {
                List<Subscriptions> subs = db.Subscriptions.Where(x => x.Active == true).ToList();
                SubscriptionSoapClient soap = new SubscriptionSoapClient("SubscriptionSoap");
                authorizeRequestBody ab = new authorizeRequestBody()
                {
                    merchantnumber = mNumber,
                    subscriptionid = 0,
                    orderid = "",
                    amount = 0,
                    currency = apiCurr,
                    instantcapture = 0,
                    fraud = 0,
                    email = apiEmail
                };

                soap.Open();

                foreach (var item in subs)
                {
                    ab.subscriptionid = item.SubscriptionIDEpay;
                    ab.orderid = item.OrderID;
                    ab.amount = item.Price * 100;

                    soap.authorize(ab.merchantnumber, ab.subscriptionid, ab.orderid, ab.amount, ab.currency, 
                        ab.instantcapture, ab.group, ab.description, ab.email, ab.sms, ab.ipaddress, ab.pwd, 
                        ab.textonstatement, ab.customerkey, ref ab.fraud, ref ab.transactionid, 
                        ref ab.pbsresponse, ref ab.epayresponse);
                }

                soap.Close();
            }
            
            return 0;
        }
    }

    public class Logger : ILog
    {
        public bool Log(LogLevel lvl, Func<string> msgFnc, Exception exception = null)
        {
            System.Diagnostics.Debug.WriteLine(msgFnc());
            return true;
        }
    }

    public class CustomHangfireLogProvider : ILogProvider
    {
        public ILog GetLogger(string name)
        {
            return new Logger();
        }
    }
}