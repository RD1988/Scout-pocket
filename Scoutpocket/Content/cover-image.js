﻿jQuery(document).ready(function ($)
{
    // Defining a function to set size for #hero 
    function fullscreen_player()
    {
        jQuery('#player').css({
            width: jQuery(window).width(),
            height: jQuery(window).height()
        });

  
    }

    function fullscreen_trainer() {
        jQuery('#trainer').css({
            width: jQuery(window).width(),
            height: jQuery(window).height()
        });


    }

    fullscreen_player();
    fullscreen_trainer();

    // Run the function in case of window resize
    jQuery(window).resize(function ()
    {
        fullscreen_player();
        fullscreen_trainer();
    });

});