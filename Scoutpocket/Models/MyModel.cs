﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models
{
    public class MyModel
    {
        public HttpPostedFileBase MyFile { get; set; }

        public string CroppedImagePath { get; set; }
    }
}