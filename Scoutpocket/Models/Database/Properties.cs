﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Scoutpocket.Models.Database
{
    public class Properties
    {

        public Properties()
        {
            //this.SubPropertiess = new HashSet<SubProperties>();
        }
   
        public int PropertiesID { get; set; }
        public string PropertiesName { get; set; }




        // public virtual Player player { get; set; }

        //One to many relation
        public virtual ICollection<SubProperties> SubProperties { get; set; }



    }


    public class SubProperties
    {
        public int SubPropertiesID { get; set; }
        public string SubPropertiesName { get; set; }

        public string SubPropertiesDescription { get; set; }
 

        public int PropertiesID { get; set; }
        public int PositionID { get; set; }

       public Properties properties { get; set; }
        //public Rating ratings { get; set; }
        public Position position { get; set; }


        //One to many relation, ratings
        public virtual ICollection<Rating> rating { get; set; }

    }

    public class Rating
    {
      
      
        public int RatingID { get; set; }
        public int PlayerID { get; set; }
        public int SubPropertiesID { get; set; }
        public string Value { get; set; }
        public int ScoutID { get; set; }


        //  public DateTime Date { get; set; }

        public DateTime? Birth { get; set; }
        public DateTime? Date { get; set; }

        
   


    }




}