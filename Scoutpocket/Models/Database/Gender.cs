﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class Gender
    {
        public int GenderID { get; set; }
        public string GenderType { get; set; }


        public virtual ICollection<SerieCategory> SerieCategories { get; set; }
    }
}