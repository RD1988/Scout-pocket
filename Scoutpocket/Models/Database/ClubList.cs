﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{

    public class GroupBy<K, T>
    {
        public K Key;
        public IEnumerable<T> Values;
    }


    public class ClubList
    {

      
        public int ClubListID { get; set; }

        [DisplayName("Klubnavn")]
        public string ClubName { get; set; }
        
        [DisplayName("Klub billede")]
        public string ClubImage { get; set; }

        [NotMapped]
        public HttpPostedFileBase MyFile { get; set; }
        [NotMapped]
        public string CroppedImagePath { get; set; }


        //public Profile profile { get; set; }
        //public Club club { get; set; }

        //One to many relation
        public virtual ICollection<Player> Player { get; set; }



    }
}