﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class View
    {

        public int ViewID { get; set; }

        public int PlayerID { get; set; }

        public int ClubID { get; set; }

    



        public DateTime Time { get; set; }
    }
}