﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class PlayerPropertyScoutComment
    {
        public int Id { get; set; }
        [Required]
        public string Comment { get; set; }
        public int PropertiesID { get; set; }
        [Required]
        public virtual Properties Property { get; set; }
        public int PlayerID { get; set; }
        [Required]
        public virtual Player Player { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [Required]
        public virtual User Scout {get;set;}
        
    }
}