﻿using Scoutpocket.Models.Database;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
 

    public class Profile
    {

        public int ProfileID { get; set; }

        [DisplayName("Fornavn")]
        public string FirstName { get; set; }

        [NotMapped]
        public HttpPostedFileBase MyFile { get; set; }
        [NotMapped]
        public string CroppedImagePath { get; set; }

        [NotMapped]
        public bool isCurrentUser { get { return HttpContext.Current.User.Identity.Name == user.UserMail; } }
        [NotMapped]
        public bool isOwningClub { get; set; }
        [DisplayName("Efternavn")]
        public string LastName { get; set; }

        [DisplayName("Telefon nummer")]
        public int Phone { get; set; }

        [DisplayName("")]
        public string Image { get; set; }

        //relations

        public virtual User user { get; set; }

        public virtual Player player { get; set; }

        public virtual Club club { get; set; }

        public virtual Subscriptions subscription {get; set;}

        [NotMapped]
        public virtual List<Properties> properties { get; set; }
        [NotMapped]
        public virtual List<SubProperties> subProperties { get; set; }
        [NotMapped]
        public virtual List<Rating> ratings { get; set; }
        [NotMapped]
        public virtual List<Profile> profiles { get; set; }












    }

}