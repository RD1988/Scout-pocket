﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Scoutpocket.Models.Database
{

    public class Group<K, T>
    {
        public K Key;
        public IEnumerable<T> Values;
    }



    public class User
    {
  
        public int UserID { get; set; }
    
        [Required(ErrorMessage = "{0} required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
         ErrorMessage = "Invalid email")]
        [Remote("CheckUserName", "User", ErrorMessage = "Already in use!")]
        [StringLength(100, ErrorMessage = "{0}: 100 is the limit")]
        public string UserMail { get; set; }




      

        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

       

        public int RoleID { get; set; }

        public virtual Profile profile { get; set; }

        //public virtual Order Order { get; set; }

   
    }
}