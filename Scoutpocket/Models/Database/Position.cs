﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public  class Position
    {
        public int PositionID { get; set; }

        public string PositionName { get; set; }

        public string PositionCategory { get; set; }

        //One to many relation
        public virtual ICollection<SubProperties> SubProperties { get; set; }




    }
}