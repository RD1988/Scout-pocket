﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class Series
    {

        public int SeriesID { get; set; }

        public string SeriesName { get; set; }

        //One to many relation
        public virtual ICollection<Player> Player { get; set; }

        public ICollection<Region> Regions { get; set; }

        public ICollection<SerieCategory> SerieCategories { get; set; }
    }
}