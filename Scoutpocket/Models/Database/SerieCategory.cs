﻿using Scoutpocket.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models
{
    public class SerieCategory
    {
        public int SerieCategoryID { get; set; }
        public string SerieCategoryName { get; set; }
        public int GenderID { get; set; }

        public ICollection<Series> Series { get; set; }

    }
}