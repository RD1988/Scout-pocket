﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{


    public class Player
    {


        //One to many relation, ratings
        public virtual ICollection<Rating> Ratings { get; set; }

        public virtual ICollection<View> Views { get; set; }


        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlayerID { get; set; }

        //public int RatingID { get; set; }

        //Foreignkey ClubList, same pk
        public int ClubListID { get; set; }

        //Foreignkey Series, same pk
        public int? SeriesID { get; set; }


        //Foreignkey Position, same pk
        public int PositionID { get; set; }

        //Foreignkey ratings, same pk
        //public int RatingsID { get; set; }


        //a relation to clublist
        public virtual ClubList Clublist { get; set; }

        //a relation to serie
        public virtual Series Series { get; set; }


        public virtual Region Region {get;set;}

     

        public Profile Profile { get; set; }



        // Birth
        public DateTime BirthDate { get; set; }

 

     
    }
}