﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class Region
    {
        [Key]
        [Column("RegionID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RegionID { get; set; }
        [Required]
        public string RegionName { get; set; }

        public ICollection<Series> Series { get; set; }

    }
}