﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scoutpocket.Models.Database
{
    public class Connection : DbContext
    {
        public Connection() : base("Connection")
        {

        }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Profile> Profiles { get; set; }

        public virtual DbSet<Club> Clubs { get; set; }

        public virtual DbSet<ClubList> ClubLists { get; set; }

        public virtual DbSet<Player> Players { get; set; }

        public virtual DbSet<View> Views { get; set; }

        public virtual DbSet<Scout> Scouts { get; set; }

        public virtual DbSet<Position> Positions { get; set; }

        public virtual DbSet<Gender> Genders { get; set; }

        public virtual DbSet<SerieCategory> SerieCategories { get; set; }

        public virtual DbSet<Series> Seriess { get; set; }

        public virtual DbSet<Properties> Propertiess { get; set; }

        public virtual DbSet<SubProperties> SubPropertiess { get; set; }

        public virtual DbSet<Subscriptions> Subscriptions { get; set; }
        public virtual DbSet<PlayerPropertyScoutComment> PlayerPropertyScoutComment{get;set;}
        //Obs
        public virtual DbSet<PreparedSubscriptions> PreparedSubscriptions { get; set; }


        public virtual DbSet<Rating> Ratings { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<Region> Regions { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {


            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>().HasRequired(u => u.profile).WithRequiredPrincipal(u => u.user).WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>().HasRequired(p => p.player).WithRequiredPrincipal(p => p.Profile).WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>().HasRequired(p => p.club).WithRequiredPrincipal(p => p.profile).WillCascadeOnDelete(false);

            modelBuilder.Entity<Profile>().HasRequired(s => s.subscription).WithRequiredPrincipal(p => p.profile).WillCascadeOnDelete(false);

            // Configure PrepId as PK for PreparedSubscriptions
            modelBuilder.Entity<PreparedSubscriptions>()
                .HasKey(e => e.PrepId);


            // Configure subscription as FK for Subscriptions
            modelBuilder.Entity<Subscriptions>()
                        .HasRequired(s => s.PreparedSubscription)
                        .WithRequiredPrincipal(p => p.subscription);



            //modelBuilder.Entity<User>().HasKey(o => o.UserID);

            //modelBuilder.Entity<Order>().HasRequired(u => u.User).WithRequiredPrincipal(o => o.Order);

        }
    }
}