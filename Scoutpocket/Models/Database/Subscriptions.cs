﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class Subscriptions
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] //set identityspecification to no
        public int SubscriptionID { get; set; }
        public int SubscriptionIDEpay { get; set; }

        public int Price { get; set; }

        public int TransactionID { get; set; }

        public string OrderID { get; set; }

        //SubscriptionsRenew
        public bool Active { get; set; }

        //0 = Basic, 1 = Physic, 2 = Mental, 3 = Both
        public int Category { get; set; }

        public Profile profile { get; set; }

        public virtual PreparedSubscriptions PreparedSubscription { get; set; }
        
    }
}