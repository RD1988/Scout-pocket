﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class Order
    {
        public int OrderID { get; set; }
        public int Price { get; set; }

        [ForeignKey("User")]
        public int UserID { get; set; }
        public string OrderIDEpay { get; set; }
        public DateTime Brought { get; set; }

        public string Product { get; set; }

        public virtual User User {get;set;}

       




    }
}