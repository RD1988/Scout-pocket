﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class SearchResult
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ClubName { get; set; }

        public int PlayerId { get; set; }
        public int ClubListId { get; set; }
        public int ProfileID { get; set; }

        public int PositionID { get; set; }
        public int Age { get; set; }

        public virtual Profile profile { get; set; }
    }
}