﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Database
{
    public class Value
    {
        public int ValueID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Contact No required!")]
        public string Values { get; set; }
    }
}