﻿using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Repository
{
    public class SubPropertieRepository : ISubPropertieRepository
    {
        private Connection db = new Connection();

        public void CreateSubProperties(SubPropertieViewModel viewModel,  Position position, Properties properties)
        {

            var subPropertie = new SubProperties()
            {
                SubPropertiesName = viewModel.SubPropertiesName,
                SubPropertiesDescription = viewModel.SubPropertiesDescription,
                PropertiesID = properties.PropertiesID,
                PositionID = position.PositionID
                
            };

            db.SubPropertiess.Add(subPropertie);

            db.SaveChanges();
        }
    }
}