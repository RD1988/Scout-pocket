﻿using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Scoutpocket.Models.Repository
{
    public class ClubRepository : IClubRepository
    {

        private Connection db = new Connection();

        public string encryption(String password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encrypt;
            UTF8Encoding encode = new UTF8Encoding();
            //encrypt the given password string into Encrypted data  
            encrypt = md5.ComputeHash(encode.GetBytes(password));
            StringBuilder encryptdata = new StringBuilder();
            //Create a new string by using the encrypted data  
            for (int i = 0; i < encrypt.Length; i++)
            {
                encryptdata.Append(encrypt[i].ToString());
            }
            return encryptdata.ToString();
        }



        /*View all clubs*/
        public IEnumerable<Club> getAllClubs()
        {

            return db.Clubs.ToList();
        }


        public void CreateClubList(ClubList viewModel)
        {
            var clubList = new ClubList()
            {
                ClubName = viewModel.ClubName
            };   
        }


        public void CreateClub(ClubProfileViewModel viewModel, int subscriptionID, int price, 
            int transactionID, string orderIDString, int category) 
        {
            var user = new User()
            {


                UserMail = viewModel.UserMail,
                UserPassword = encryption(viewModel.UserPassword),
                RoleID = 2


            };

            var profile = new Profile()
            {
                FirstName = viewModel.FirstName,
                Phone = viewModel.Phone,
                
               
            };


            var club = new Club()
            {
                ClubListID = viewModel.ClubListID
            };

            //This should be made into order instead.
            var subscription = new Subscriptions()
            {
                OrderID = orderIDString,
                TransactionID = transactionID,
                Price = price / 100,
                SubscriptionIDEpay = subscriptionID,
                Active = true,
                Category = category
                
                
            };

           
            

            //Makes a relation 
            user.profile = profile;
            club.profile = profile;
            club.profile.subscription = subscription;
            profile.subscription = subscription;

            db.Users.Add(user);
            db.Profiles.Add(profile);
            db.Clubs.Add(club);
            db.Subscriptions.Add(subscription);

            db.SaveChanges();




        }



        public void CreateNonPayingClub(ClubProfileViewModel viewModel, int category, int price)
        {
            var user = new User()
            {


                UserMail = viewModel.UserMail,
                UserPassword = encryption(viewModel.UserPassword),
                RoleID = 2


            };

            var profile = new Profile()
            {
                FirstName = viewModel.FirstName,
                Phone = viewModel.Phone,


            };


            var club = new Club()
            {
                ClubListID = viewModel.ClubListID
            };

            //This should be made into order instead.
            var subscription = new Subscriptions()
            {
                OrderID = "free order",
                TransactionID = 0,
                Price = price / 100,
                SubscriptionIDEpay = 0,
                Active = true,
                Category = category

            };




            //Makes a relation 
            user.profile = profile;
            club.profile = profile;
            club.profile.subscription = subscription;
            profile.subscription = subscription;

            db.Users.Add(user);
            db.Profiles.Add(profile);
            db.Clubs.Add(club);
            db.Subscriptions.Add(subscription);

            db.SaveChanges();
        }

        public void CreateORUpdatePreparedClubSubscription(int targetId, int price, bool active, int category)
        {
            PreparedSubscriptions result = db.PreparedSubscriptions.SingleOrDefault(x => x.PrepId == targetId);
            Subscriptions result2 = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == targetId);

            if (result2 == null)
            {
                return;
            }

            // If one exists, update
            if (result != null)
            {
                result.SubscriptionIDEpay = result2.SubscriptionIDEpay;
                result.Price = price;
                result.TransactionID = result2.TransactionID;
                result.OrderID = result2.OrderID;
                result.Active = active;
                result.Category = category;

                db.SaveChanges();
                return;
            }


            // If not, create new
            var prepObj = new PreparedSubscriptions()
            {
                SubscriptionIDEpay = result2.SubscriptionIDEpay,
                Price = price,
                TransactionID = result2.TransactionID,
                OrderID = result2.OrderID,
                Active = active,
                Category = category
            };

            prepObj.subscription = result2;

            db.PreparedSubscriptions.Add(prepObj);

            db.SaveChanges();
        }

        public void CreateORUpdatePreparedClubSubscription(int targetId, int subscriptionID, int price, int transactionID, string orderID, bool active, int category)
        {
            //the reason for this is because db.PreparedSubscriptions.SingleOrDefault gave the following error:
            //Subscription_SubscriptionID column does not exist.
            //since preparedsubscription does not exist yet, so I just make one.

            var result = db.PreparedSubscriptions.SingleOrDefault(x => x.PrepId == targetId);

            var result2 = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == targetId);

            if (result2 == null)
            {
                //error? 
                return;
            }

            // If one exists, update
            if (result != null)
            {
                result.SubscriptionIDEpay = subscriptionID;
                result.Price = price;
                result.TransactionID = transactionID;
                result.OrderID = orderID;
                result.Active = active;
                result.Category = category;

                db.SaveChanges();
                return;
            }
            // If none exists, create new
            var prepObj = new PreparedSubscriptions()
            {
                SubscriptionIDEpay = subscriptionID,
                Price = price,
                TransactionID = transactionID,
                OrderID = orderID,
                Active = active,
                Category = category
            };




            prepObj.subscription = result2;

            db.PreparedSubscriptions.Add(prepObj);

            db.SaveChanges();
        }

        public IEnumerable<Profile> SearchPlayers(string FirstName)
        {


            var s = db.Profiles.Where(x => x.FirstName.Contains(FirstName) && x.user.RoleID == 3);

            return s;

        }






    }
}