﻿using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Data.Entity ;
using System.Web;
using System.Text;
using System.Security.Cryptography;

namespace Scoutpocket.Models.Repository
{
    public class ScoutRepository : IScoutRepository
    {
        private Connection db = new Connection();

        public string encryption(String password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encrypt;
            UTF8Encoding encode = new UTF8Encoding();
            //encrypt the given password string into Encrypted data  
            encrypt = md5.ComputeHash(encode.GetBytes(password));
            StringBuilder encryptdata = new StringBuilder();
            //Create a new string by using the encrypted data  
            for (int i = 0; i < encrypt.Length; i++)
            {
                encryptdata.Append(encrypt[i].ToString());
            }
            return encryptdata.ToString();
        }



        public void CreateScout(ScoutProfileViewModel viewModel)
        {
            var user = new User()
            {


                UserMail = viewModel.UserMail,
                UserPassword = encryption(viewModel.UserPassword),
                RoleID = 4


            };

            var profile = new Profile()
            {
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName



            };

            var scout = new Scout()
            {
             
            };

            //Makes a relation 
            user.profile = profile;
            scout.profile = profile;


            db.Users.Add(user);
            db.Profiles.Add(profile);
            db.Scouts.Add(scout);



            db.SaveChanges();


            GetMail(viewModel);

        }


        public IEnumerable<User> GetAllScouts()
        {
            return db.Users.ToList();
        }

        public IEnumerable<Profile> SearchPlayers(string search)
        {
            
            
            var s = db.Profiles.Where(x => x.FirstName.Contains(search)  && x.user.RoleID  == 3);
             
            return s;

        }

        public Profile GetProfileById(int? id)
        {
            return db.Profiles.SingleOrDefault(x => x.player.PlayerID == id);
        } 

        public void GetMail(ScoutProfileViewModel viewModel)
        {
            MailMessage mailMessage = new MailMessage();

            MailAddress fromAddress = new MailAddress("noreply@scoutpocket.dk");

            mailMessage.From = fromAddress;

            mailMessage.To.Add(viewModel.UserMail);

            mailMessage.Body = "Velkommen til scoutpocket, du er nu blevet oprettet som  scout ved og har følgende fordele bla. bla. Du kan logge ind med din adgangskode: " + viewModel.UserPassword + "ved at følge dette link *Link her*";
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = "Scoutpocket, klub registering";

            SmtpClient smtpClient = new SmtpClient();

            smtpClient.Host = "smtp.unoeuro.com";

            smtpClient.Port = 25;

            smtpClient.Send(mailMessage);

        }

        //public IEnumerable<Properties> GetAllProperties()
        //{
            


        //    return db.Propertiess.Include(a => a.SubPropertiess.Select(c => c.properties));
        //}

    }
}