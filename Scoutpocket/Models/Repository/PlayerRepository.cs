﻿using Scoutpocket.Models.Database;
using Scoutpocket.Models.Interface;
using Scoutpocket.ViewModels;
using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
namespace Scoutpocket.Models.Repository
{
    public class PlayerRepository : IPlayerRepository
    {
        private Connection db = new Connection();

 
        public string encryption(String password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encrypt;
            UTF8Encoding encode = new UTF8Encoding();
            //encrypt the given password string into Encrypted data  
            encrypt = md5.ComputeHash(encode.GetBytes(password));
            StringBuilder encryptdata = new StringBuilder();
            //Create a new string by using the encrypted data  
            for (int i = 0; i < encrypt.Length; i++)
            {
                encryptdata.Append(encrypt[i].ToString());
            }
            return encryptdata.ToString();
        }




        public void CreatePlayer(PlayerProfileViewModel viewModel, Player player, int subscriptionEPAYID, int price, int transactionID, string orderID, string product, bool isActive, int subscriptionCategory, Region region)
        {

            var user = new User()
            {
                //the encryption should be on: - Hamun
                UserMail = viewModel.UserMail,
                //UserPassword = encryption(viewModel.UserPassword),
                UserPassword = viewModel.UserPassword,
                RoleID = 3
            };

            var profile = new Profile()
            {
                
                FirstName = viewModel.FirstName,
                Image = "User.jpg",
                LastName = viewModel.LastName

            };

            var addPlayer = new Player()
            {
                
                ClubListID = player.ClubListID,
                PositionID = player.PositionID,

                SeriesID = player.SeriesID,

                BirthDate = player.BirthDate

            };


            //  ((List<Player>)player.clublist.Players).Add(player);

            //make the subscription
            var subscription = new Subscriptions()
            {
                SubscriptionIDEpay = subscriptionEPAYID,
                Price = price,
                OrderID = orderID,
                TransactionID = transactionID,
                Active = isActive,
                Category = subscriptionCategory

            };


            var prepSubscription = new PreparedSubscriptions()
            {
                SubscriptionIDEpay = subscriptionEPAYID,
                Price = price,
                OrderID = orderID,
                TransactionID = transactionID,
                Active = true,
                Category = subscriptionCategory

            };

            /*
            var order = new Order()
            {
                OrderIDEpay = orderID,
                Price = price / 100,
                Product = product,
                Brought = DateTime.Now,
                UserID = user.UserID
            };
            */


            //Makes a relation 
            user.profile = profile;
            //user.Order = order;
            addPlayer.Profile = profile;
            subscription.profile = profile;
            //order.User = user;
            addPlayer.Region = db.Regions.Single(r => r.RegionID == region.RegionID);

            //db.Orders.Add(order);
            db.Users.Add(user);
            db.Profiles.Add(profile);
            db.Players.Add(addPlayer);
            
            db.Subscriptions.Add(subscription);
            db.SaveChanges();
            if (isActive == false)
            {
                prepSubscription.subscription = subscription;
                db.PreparedSubscriptions.Add(prepSubscription);
            }

            //The below error does not exsist anymore maybe?
            //the same error happens here: that the subscription_SubscriptionID does not exist

            db.SaveChanges();


        }

        public void CreateNonPayingPlayer(PlayerProfileViewModel viewModel, Player player, int subscriptionCategory, Region region)
        {

            var user = new User()
            {
                //the encryption should be on: - Hamun
                UserMail = viewModel.UserMail,
                //UserPassword = encryption(viewModel.UserPassword),
                UserPassword = viewModel.UserPassword,
                RoleID = 3

            };

            var profile = new Profile()
            {
                FirstName = viewModel.FirstName,
                Image = "User.jpg",
                LastName = viewModel.LastName

            };

            var addPlayer = new Player()
            {
                ClubListID = player.ClubListID,
                PositionID = player.PositionID,
                SeriesID = player.SeriesID,
                BirthDate = player.BirthDate
            };

            //make the subscription
            var subscription = new Subscriptions()
            {
                SubscriptionIDEpay = 0,
                Price = 0,
                OrderID = "free order",
                TransactionID = 0,
                Active = true,
                Category = subscriptionCategory
            };

            //Makes a relation 
            user.profile = profile;
            //user.Order = order;
            addPlayer.Profile = profile;
            subscription.profile = profile;
            //order.User = user;
            addPlayer.Region = db.Regions.Single(r => r.RegionID == region.RegionID);

            //db.Orders.Add(order);
            db.Users.Add(user);
            db.Profiles.Add(profile);
            db.Players.Add(addPlayer);
            db.Subscriptions.Add(subscription);


            db.SaveChanges();
        }

        public void CreateORUpdatePreparedSubscription(int targetId, int price, bool active, int category)
        {
            PreparedSubscriptions result = db.PreparedSubscriptions.SingleOrDefault(x => x.PrepId == targetId);
            Subscriptions result2 = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == targetId);

            if (result2 == null)
            {
                return;
            }

            // If one exists, update
            if (result != null)
            {
                result.SubscriptionIDEpay = result2.SubscriptionIDEpay;
                result.Price = price;
                result.TransactionID = result2.TransactionID;
                result.OrderID = result2.OrderID;
                result.Active = active;
                result.Category = category;

                db.SaveChanges();
                return;
            }


            // If not, create new
            var prepObj = new PreparedSubscriptions()
            {
                SubscriptionIDEpay = result2.SubscriptionIDEpay,
                Price = price,
                TransactionID = result2.TransactionID,
                OrderID = result2.OrderID,
                Active = active,
                Category = category
            };

            prepObj.subscription = result2;

            db.PreparedSubscriptions.Add(prepObj);

            db.SaveChanges();
        }

        public void CreateORUpdatePreparedSubscription(int targetId, int subscriptionID, int price, int transactionID, string orderID, bool active, int category)
        {
            //the reason for this is because db.PreparedSubscriptions.SingleOrDefault gave the following error:
            //Subscription_SubscriptionID column does not exist.
            //since preparedsubscription does not exist yet, so I just make one.

            var result = db.PreparedSubscriptions.SingleOrDefault(x => x.PrepId == targetId);
           
            var result2 = db.Subscriptions.SingleOrDefault(x => x.SubscriptionID == targetId);

            if (result2 == null)
            {
                //error? 
                return;
            }

            // If one exists, update
            if (result != null)
            {
                result.SubscriptionIDEpay = subscriptionID;
                result.Price = price;
                result.TransactionID = transactionID;
                result.OrderID = orderID;
                result.Active = active;
                result.Category = category;

                db.SaveChanges();
                return;
            }
            // If none exists, create new
            var prepObj = new PreparedSubscriptions()
            {
                SubscriptionIDEpay = subscriptionID,
                Price = price,
                TransactionID = transactionID,
                OrderID = orderID,
                Active = active,
                Category = category
            };




            prepObj.subscription = result2;

            db.PreparedSubscriptions.Add(prepObj);

            db.SaveChanges();
        }


        public IEnumerable<Profile> getUserByID()
        {


            return db.Profiles.ToList();
        }

        public Profile GetPlayerByID(int id)
        {
            return db.Profiles.FirstOrDefault(p => p.ProfileID == id);
        }



    }
}