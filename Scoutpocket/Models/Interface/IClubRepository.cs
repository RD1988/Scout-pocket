﻿using Scoutpocket.Models.Database;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Interface
{
    public interface IClubRepository
    {

        IEnumerable<Club> getAllClubs();

        void CreateClub(ClubProfileViewModel viewModel, int subscriptionID, 
            int price, int transactionID, string orderIDString, int category) ;


        void CreateORUpdatePreparedClubSubscription(int targetId, int price, bool active, int category);
        void CreateNonPayingClub(ClubProfileViewModel viewModel, int category, int price);
        void CreateORUpdatePreparedClubSubscription(int targetId, int subscriptionID, int price, int transactionID, string orderID, bool active, int category);


        void CreateClubList(ClubList viewModel);

    }
}