﻿using Scoutpocket.Models.Database;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Interface
{
    public interface IScoutRepository
    {
        void CreateScout(ScoutProfileViewModel viewModel);

        IEnumerable<User> GetAllScouts();

        IEnumerable<Profile> SearchPlayers(string search);

        Profile GetProfileById(int? id);

        //IEnumerable<Properties> GetAllProperties();
    }
}