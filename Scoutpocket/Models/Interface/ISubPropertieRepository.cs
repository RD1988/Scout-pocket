﻿using Scoutpocket.Models.Database;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Interface
{
    public interface ISubPropertieRepository
    {
        void CreateSubProperties(SubPropertieViewModel viewModel, Position position, Properties properties);
    }
}