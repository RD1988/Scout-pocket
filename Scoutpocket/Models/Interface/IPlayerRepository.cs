﻿using Scoutpocket.Models.Database;
using Scoutpocket.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoutpocket.Models.Interface
{
    public interface IPlayerRepository
    {
        void CreatePlayer(PlayerProfileViewModel viewModel, 
            Player player, int subscriptionID, int price, int transactionID, 
            string orderID, string product, bool isActive, int subscriptionCategory, Region region);
        IEnumerable<Profile> getUserByID();

        void CreateNonPayingPlayer(PlayerProfileViewModel viewModel, Player player, int subscriptionCategory, Region region);

        /// <summary>
        /// Creates or updates a PreparedSubscription entry using existing Subscription properties.
        /// </summary>
        /// <param name="targetId">Primary Key</param>
        /// <param name="price">New Price</param>
        /// <param name="active">Payment active</param>
        /// <param name="category">Subscription Category</param>

        void CreateORUpdatePreparedSubscription(int targetId, int price, bool active, int category);
        /// <summary>
        /// Creates or updates a PreparedSubscription entry using new payment info
        /// </summary>
        /// <param name="targetId"></param>
        /// <param name="subscriptionID"></param>
        /// <param name="price"></param>
        /// <param name="transactionID"></param>
        /// <param name="orderID"></param>
        /// <param name="active"></param>
        /// <param name="category"></param>

        void CreateORUpdatePreparedSubscription(int targetId, int subscriptionID, int price, int transactionID, string orderID, bool active, int category);


        Profile GetPlayerByID(int id);

    }
}